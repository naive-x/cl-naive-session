(defsystem "cl-naive-session.ssl"
  :description "Session layer over ssl."
  :version "2023.8.3"
  :author "Phil Marneweck <phil@psychedelic.co.za>"
  :licence "MIT"
  :depends-on (:cl-naive-session
               :cl+ssl)
  :components ((:file "src/transport-ssl")))

