(defpackage :cl-naive-session.tests.session.webserver
  (:use
   :common-lisp
   :cl-naive-webserver
   :cl-naive-session
   :cl-naive-session.tests.session.global)
  (:shadowing-import-from :cl-naive-session
   :session)
  (:export
   :main
   :create-server
   :create-site
   :add-handler))
(in-package :cl-naive-session.tests.session.webserver)

(defvar *port*)
(defvar *server* nil)
(defvar *test-site* nil)

(defun create-server (&key (port *port*) (address "localhost"))
  (let ((server (make-instance 'cl-naive-webserver.hunchentoot:hunchentoot-server
		               :address address
		               :port port)))
    (register-server server)
    (start-server server)
    server))

(defun create-site (url)
  (let ((site (make-instance 'site :url url)))
    (register-site site)
    site))

(defmethod add-handler ((site site) (subpath string) (handler function))
  (setf (cl-naive-webserver:find-resource site subpath) handler))

(defun main ()
  (setf *server* (create-server :port *port*))
  (setf *test-site* (create-site "/test"))
  (add-handler *test-site* "/home" (lambda (script-name)
                                     (declare (ignore script-name))
                                     "Yeeeha she is alive and kicking!"))
  (format t "http://localhost:~A/test/home~%" *port*)
  (values))


;;;; THE END ;;;;
