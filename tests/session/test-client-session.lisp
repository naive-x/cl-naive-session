(defpackage :cl-naive-session.tests.session.unit-tests
  (:use :cl :cl-naive-tests :cl-naive-session)
  (:export))
(in-package :cl-naive-session.tests.session.unit-tests)

;; Define here session unit tests.



;;;
;;; CLIENT-SESSION
;;; 


(defclass mock-http-drakma-transport (cl-naive-session::multiple-sessions-mixin
                                      cl-naive-session::http-drakma-transport)
  ())


(defmethod cl-naive-session::send-packet ((transport mock-http-drakma-transport) (packet packet))
  (format *trace-output* "SEND-PACKET    ~A ~A" transport packet)
  (force-output *trace-output*)
  (let ((session (cl-naive-session::get-session-by-id transport (session-id packet))))
    (cl-naive-session::enqueue
     (cl-naive-session::response-queue transport)
     (if session
	     (merge-packets (cl-naive-session::session-response-ok session)
                        (make-instance 'packet :message '(response)))
         (merge-packets (cl-naive-session::session-response-reject nil)
                        (make-instance 'packet :message `(no session for ,(session-id packet))))))))


(testsuite :client-session
  (testcase :check-state
            :expected '(:after open-session
                        :state :c-virgin
                        :has-session-id t
                        :after send
                        :state :c-opening
                        :after response
                        :state :c-idle
                        :response (:session-status "ok" :message (response))
                        :after send
                        :state :c-waiting
                        :after response
                        :state :c-idle
                        :response (:session-status "ok" :message (response))
                        :after close-session
                        :state :deleted)

            :equal 'equalp
            :actual (let* ((*trace-output* (make-broadcast-stream))
                           (cl-naive-session::*client-transports*
                             (append '((:http mock-http-drakma-transport nil))
                                     cl-naive-session::*client-transports*))
                           (session (open-session "http://localhost:8080/ms/test-client-session")))
                      (list*
                       :after 'open-session
                       :state (session-state session)
                       :has-session-id (not (null (session-id session)))
                       :after 'send
                       (progn
                         (send session (make-instance 'packet :message '(:type "greeting" :content "Hello")))
                         (list* :state (session-state session)
                                (let ((response (receive session)))
                                  (list*
                                   :after 'response
                                   :state (session-state session)
                                   :response (list :session-status (cl-naive-session::session-status response)
                                                   :message (packet-message response))
                                   (progn (send session (make-instance 'packet :message '(:type "request" :content "gime gime 0")))
                                          (list* :after 'send
                                                 :state (session-state session)
                                                 (let ((response (receive session)))
                                                   (list*
                                                    :after 'response
                                                    :state (session-state session)
                                                    :response (list :session-status (cl-naive-session::session-status response)
                                                                    :message (packet-message response))
                                                    (progn (close-session session)
                                                           (list*
                                                            :after 'close-session
                                                            :state (session-state session)
                                                            '()))))))))))))
            :info "check client-session states"))


;;;; THE END ;;;;
