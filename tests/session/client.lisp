(defpackage :cl-naive-session.tests.session.client
  (:use :common-lisp
   :cl-naive-session
   :cl-naive-session.tests.session.global)
  (:export :start :stop :run-client-request))
(in-package :cl-naive-session.tests.session.client)


(defvar *session* nil)

(defun start ()
  (unless *session*
    (setf *session* (open-session (format nil "http://localhost:~A/ms/test" *port*)))))

(defun stop ()
  (when *session*
    (close-session *session*)
    (setf *session* nil)))

(defun run-client-request ()
  (format *trace-output* "1 session state = ~S~%" (session-state *session*))
  (handler-case
      (send *session* (make-instance 'packet
                                     :parameters '(:message-kind "test")
                                     :message '(:test :test :test)))
    (error (err)
      (format *trace-output* "Error while sending message:   ~A~%" err)
      (format *trace-output* "1.e session state = ~S~%" (session-state *session*))
      (force-output *trace-output*)
      (return-from run-client-request nil)))
  (format *trace-output* "2 session state = ~S~%" (session-state *session*))
  (force-output *trace-output*)
  (prog1  (handler-case
              (receive *session*)
            (error (err)
              (format *trace-output* "Error while receiving message: ~A~%" err)
              (format *trace-output* "2.e session state = ~S~%" (session-state *session*))
              (force-output *trace-output*)
              (return-from run-client-request nil)))
    (format *trace-output* "3 session state = ~S~%" (session-state *session*))
    (force-output *trace-output*)))

