(defpackage :cl-naive-session.tests.session.global
  (:use :common-lisp)
  (:export
   :*port*
   :tracing-handler
   :trace-handler))
(in-package :cl-naive-session.tests.session.global)

(defvar *port* 33037)

(defun get-handler (site subpath)
  (gethash subpath
           (cl-naive-webserver:handlers
            (cl-naive-webserver:get-site site))))

(defun tracing-handler (site subpath)
  (let ((handler (get-handler site subpath)))
    (unless handler
      (error "No handler for ~A ~S" site subpath))
    (lambda (script-name)
      (format t "~&Will call handler for ~S~%" script-name)
      (finish-output)
      (unwind-protect
           (funcall handler script-name)
        (format t "~&Did call handler for ~S~%" script-name)
        (finish-output)))))

(defun trace-handler (site subpath)
  (setf (cl-naive-webserver:find-resource (cl-naive-webserver:get-site site)
                                          subpath)
        (tracing-handler site subpath)))
