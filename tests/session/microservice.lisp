(defpackage :cl-naive-session.tests.session.microservice
  (:use
   :common-lisp
   :cl-naive-webserver
   :cl-naive-session
   :cl-naive-session.tests.session.global)
  (:shadowing-import-from :cl-naive-session
   :session)
  (:export ))
(in-package :cl-naive-session.tests.session.microservice)


(defvar *session* nil)

(defun start ()
  (unless *session*
    (setf *session* (accept-session (format nil "http://localhost:~A/ms/test" *port*)))))

(defun stop ()
  (when *session*
    (close-session *session*)
    (setf *session* nil)))


(defun microservice-/ms/test ()
  (loop
    :for counter :from 1
    :for message := (receive *session*)
    :for request := (packet-message message)
    :for request-parameters := (packet-parameters message)
    :for response := (list :received counter :request request)
    :for response-parameters := (list* :message-type :response request-parameters)
    :do (format t "~A Received message: ~S ~S~%" 'microservice-/ms/test
                request request-parameters)
        (format t "~A Sending  message: ~S ~S~%" 'microservice-/ms/test
                response response-parameters)
        (finish-output)
        (send *session* response response-parameters)
    :until (eql request :stop)))

