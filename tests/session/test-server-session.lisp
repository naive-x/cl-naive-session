(defpackage :cl-naive-session.tests.session.unit-tests
  (:use :cl :cl-naive-tests :cl-naive-session)
  (:export))
(in-package :cl-naive-session.tests.session.unit-tests)

;; Define here session unit tests.

;;;
;;; SERVER-SESSION
;;; 

(defclass mock-http-webserver-transport (cl-naive-session::http-webserver-transport)
  ())

(defmethod cl-naive-session::handle-request ((transport mock-http-webserver-transport)
	                                     webserver-request
                                             script-name
                                             request)
  (declare (ignore webserver-request script-name))
  (setf *print-right-margin* nil)
  ;; TODO: in case of error, we need to still mailbox-post/mailbox-collect.
  ;; (format *trace-output* "handle-request mock-http-webserver-transport: request = ~A~%" request) (force-output *trace-output*)
  (let ((packet (cl-naive-session::decode-packet
                 transport
                 (cons (append (getf request :get-parameters)
                               (getf request :post-parameters))
                       (cl-naive-session::read-payload request)))))
    ;; (format *trace-output* "handle-request: packet = ~A~%" packet) (force-output *trace-output*)

    (let ((session (cl-naive-session::get-session-by-id transport (session-id packet))))	
      ;; (format *trace-output* "handle-request: command = ~A ~A~%" (cl-naive-session::session-command packet) (session-id packet))
      (when (and (null session)
		 (equalp "create" (cl-naive-session::session-command packet)))
        (setf session (cl-naive-session::get-session-by-id transport nil))
        (setf (session-id session) (session-id packet))
        (cl-naive-session::register-session transport session)
        ;; (format *trace-output* "handle-request: created session ~A -> ~A~%" (session-id packet) session)
        )
      ;; (format *trace-output* "handle-request: session = ~A~%" session)  (force-output *trace-output*)
      (let* ((status 200)
             (response (if session
                           (progn
                             (cl-naive-session::mailbox-post (cl-naive-session::request-mailbox session) packet)
                             ;; (format *trace-output* "handle-request: request posted, wait for response ~%") (force-output *trace-output*)
                             ;; wait for a response in response-mailbox
                             (cl-naive-session::mailbox-collect (cl-naive-session::response-mailbox session)))
                           (progn
                             ;; (format *trace-output* "handle-request: no session, send a reject response ~%") (force-output *trace-output*)
                             ;; send a negative response
                             (setf status 404)
                             (cl-naive-session::session-response-reject (session-id packet)
                                                                        (if (session-id packet)
                                                                            "Session ID unknown."
                                                                            "Missing a Session ID in headers."))))))
        ;; (format *trace-output* "handle-request: response = ~A~%" response) (force-output *trace-output*)
        (let* ((encoded (cl-naive-session::encode-packet transport response)))
          ;; (format *trace-output* "handle-request: encoded = ~A~%" encoded) (force-output *trace-output*)
	  (setf (cl-naive-webserver:headers cl-naive-webserver:*reply*)
	        (append `((:content-length . ,(length (cdr encoded)))
                          (:content-type . "application/octet-stream")
                          (:status . ,status))
                        (car encoded)
                        (cl-naive-webserver:headers cl-naive-webserver:*reply*)))

          ;; (format *trace-output* "raw-data = ~S~%"
          ;;         (slot-value hunchentoot::*request* 'hunchentoot::raw-post-data))
          ;; (force-output *trace-output*)
          
          (cdr encoded))))))

(defvar *session* nil)

(defun client-thread ()
  (sleep 1)
  (with-standard-io-syntax
    (let ((*print-readably* nil))
      (format *trace-output* "client-thread: ~A started~%" (bt:current-thread))
      (force-output *trace-output*)
      (loop
        :with client-session := (let ((cl-naive-session::*client-transports*
                                        (append '((:http mock-http-drakma-transport nil))
                                                cl-naive-session::*client-transports*)))
                                  (open-session "http://localhost:39999/ms"))
        :for packet :in (list
                         (merge-packets (cl-naive-session::session-request-create client-session)
                                        (make-instance 'packet :message '(:type "request" :parameter "gime gime 1")))
                         (merge-packets (cl-naive-session::session-request-message client-session)
                                        (make-instance 'packet :message '(:type "request" :parameter "gime gime 2")))
                         ;; the third packet will receive the session closing command.
                         (merge-packets (cl-naive-session::session-request-message client-session)
                                        (make-instance 'packet :message '(:type "request" :parameter "gime gime 3"))))
        :for cl-naive-webserver:*request*
        := (make-instance 'cl-naive-webserver:request
                          :query-string "/ms/test-server-session"
                          :remote-port 29999
                          :remote-address "localhost"
                          :server-protocol :http
                          :script-name "/test-server-session"
                          :uri "http://localhost:39999/ms/test-server-session"
                          :headers `((:host . "localhost:39999")
                                     (:connection . "keep-alive")
                                     (:accept . "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8")
                                     (:user-agent . "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36")
                                     (:accept-encoding . "gzip, deflate, sdch")
                                     (:accept-language . "en-US,en;q=0.8,fr;q=0.6")
                                     (:cookie . "cl-naive-session=1")
                                     (:cache-control . "max-age=0")
                                     (:upgrade-insecure-requests . "1")))
        :for cl-naive-webserver:*reply* := (make-instance 'cl-naive-webserver:reply
                                                          :headers '()
                                                          :return-code 200)
        :for (parameters . data) := (cl-naive-session::encode-packet (cl-naive-session::transport client-session) packet)
        :do (sleep 2)
            (format *trace-output* "client-thread: ~A handling request ~S ~S~%" (bt:current-thread) parameters data)
            (force-output *trace-output*)
            (cl-naive-session::handle-request
	     (cl-naive-session::transport *session*)
             cl-naive-webserver:*request* 
             "/test-server-session" 
             (list :headers-in      (cl-naive-webserver:headers cl-naive-webserver:*request*)
                   :request-method  "POST"
                   :request-uri     "http://localhost:39999/ms/test-server-session"
                   :server-protocol "HTTP/1.1"
                   :local-addr      "localhost"
                   :local-port      39999
                   :remote-addr     "localhost"
                   :remote-port     29999
                   :content-stream  (make-string-input-stream "")
                   :post-data       data
                   :cookies-in      nil
                   :get-parameters  parameters
                   :post-parameters parameters
                   :script-name     "/test-server-session"
                   :query-string    "/ms/test-server-session"
                   :session         nil
                   :aux-data        nil)))
      (format *trace-output* "client-thread: ~A finished~%" (bt:current-thread))
      (force-output *trace-output*))))

(testsuite :server-session
  (testcase :check-state
            :expected '(:AFTER ACCEPT-SESSION
                        :STATE :S-VIRGIN
                        :HAS-SESSION-ID NIL
                        :AFTER RECEIVE
                        :STATE :S-OPENING
                        :HAS-SESSION-ID T
                        :REQUEST (:MESSAGE (:TYPE "request" :PARAMETER "gime gime 1"))
                        :AFTER SEND
                        :STATE :S-WAITING
                        :AFTER REQUEST
                        :STATE :S-IDLE
                        :REQUEST (:MESSAGE (:TYPE "request" :PARAMETER "gime gime 2"))
                        :AFTER SEND
                        :STATE :S-WAITING
                        :AFTER CLOSE-SESSION
                        :STATE :DELETED)
            :equal 'equalp
            :actual (let* ((*trace-output* *trace-output* ;; (make-broadcast-stream)
                                           )
                           (cl-naive-session::*server-transports*
                             (append '((:http mock-http-webserver-transport nil))
                                     cl-naive-session::*server-transports*))
                           (session (accept-session "http://localhost:39999/ms/test-server-session"
                                                    :site     (let ((site (make-instance 'cl-naive-webserver:site
                                                                                         :url "http://localhost:39999/ms")))
                                                                (cl-naive-webserver:register-site site)
                                                                site)
                                                    :resource "/test-server-session"))
                           (client-thread
                             (bt:make-thread  (function client-thread)
                                              :name "client-thread http://localhost:39999/ms/test-server-session"
				              :initial-bindings `((*session* . ,session)
                                                                  (*trace-output* . ,*trace-output*)
                                                                  (*error-output* . ,*error-output*)
                                                                  (*standard-output* . ,*standard-output*)
                                                                  (*pretty-print* . nil)
	                                                          (*print-right-margin* . nil)))))
                      (format t "server: accept-session~%") (force-output)
                      (unwind-protect
                           (list*
                            :after 'accept-session
                            :state (session-state session)
                            :has-session-id (not (null (session-id session)))
                            (let ((request (receive session)))
                              (format t "server: received ~S~%" request) (force-output)
                              (list*
                               :after 'receive
                               :state (session-state session)
                               :has-session-id (not (null (session-id session)))
                               :request (list :message (packet-message request))
                               (let ((packet (make-instance 'packet :message '(:type "response" :content "take take 1"))))
                                 (send session packet)
                                 (format t "server: sent ~S~%" packet) (force-output)
                                 (list* :after 'send
                                        :state (session-state session)
                                        (let ((request (receive session)))
                                          (format t "server: received ~S~%" request) (force-output)
                                          (list*
                                           :after 'request
                                           :state (session-state session)
                                           :request (list :message (packet-message request))
                                           (let ((packet (make-instance 'packet :message '(:type "response" :content "take take 2"))))
                                             (send session packet)
                                             (format t "server: sent ~S~%" packet) (force-output)
                                             (list* :after 'send
                                                    :state (session-state session)
                                                    (progn (close-session session)
                                                           (list*
                                                            :after 'close-session
                                                            :state (session-state session)
                                                            '())))))))))))
                        (sleep 5)
                        (ignore-errors
                         ;; the client-thread should already have finished
                         (when (bt:thread-alive-p client-thread)
                           (bt:destroy-thread client-thread)))))
            :info "check server-session state"
            :disabled nil))
