(defpackage :cl-naive-session.tests.common
  (:use :common-lisp)
  (:export :set-equalp
           :plist-getf
           :alist-getf
           :alist-equalp
           :plist-equalp
           :result-equalp))
(in-package :cl-naive-session.tests.common)

(defun set-equalp (a b)
  (and (subsetp a b :test (function equalp))
       (subsetp b a :test (function equalp))))

(defun plist-getf (plist indicator &optional default)
  "Like GETF but with EQUALP"
  (loop :for (key value) :on plist :by (function cddr)
        :when (equalp key indicator)
        :do (return-from plist-getf value))
  default)

(defun alist-getf (alist indicator &optional default)
  "Like CAR ASSOC but with EQUALP"
  (car (or (assoc indicator alist :test (function equalp))
           (list default))))

(defun alist-equalp (a b)
  (let ((ka (mapcar (function car) a))
	    (kb (mapcar (function car) b)))
    (if (set-equalp ka kb)
        (loop :for k :in ka
              :for va := (alist-getf a k '#:not-in-a)
              :for vb := (alist-getf b k '#:not-in-b)
              :unless (equalp va vb)
              :do (format *trace-output* "Values for key ~S are different: ~%~S~%~S~%"
                          k va vb)
              :always (equalp va vb))
        (progn
          (format *trace-output* "Key sets are different: ~%~S~%~S~%"
                  ka kb)
          nil))))

(defun plist-equalp (a b)
  (let ((ka (loop :for (key) :on a :by (function cddr)
		  :collect key))
	(kb (loop :for (key) :on b :by (function cddr)
		  :collect key)))
    (if (set-equalp ka kb)
        (loop :for k :in ka
              :for va := (plist-getf a k '#:not-in-a)
              :for vb := (plist-getf b k '#:not-in-b)
              :unless (equalp va vb)
              :do (format *trace-output* "Values for key ~S are different: ~%~S~%~S~%"
                          k va vb)
              :always (equalp va vb))
        (progn
          (format *trace-output* "Key sets are different: ~%~S~%~S~%"
                  ka kb)
          nil))))

(defun result-equalp (a b)
  (and (cond
         ((and (consp (first (car a)))
               (consp (first (car b))))
          (alist-equalp (car a) (car b)))
         ((and (atom (first (car a)))
               (atom (first (car b))))
          (plist-equalp (car a) (car b)))
         (t (format *trace-output*
                    "First elements are not both plists or both alists: ~%~S~%~S~%"
                    (car a) (car b))
            nil))
       (equalp (cdr a) (cdr b))))
