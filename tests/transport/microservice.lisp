(defpackage :cl-naive-session.tests.transport.microservice
  (:use
   :common-lisp
   :cl-naive-webserver
   :cl-naive-session
   :cl-naive-session.tests.transport.webserver)
  (:shadowing-import-from :cl-naive-session
   :session)
  (:export ))
(in-package :cl-naive-session.tests.transport.microservice)

(defun create-microservice-transport-server (server site-url subpath)
  (let* ((local-url (format nil "http://~A:~A~A~A" (address server) (port server) site-url subpath))
         (transport (cl-naive-session::create-server-transport local-url :webserver server))
         (ms-site   (create-site site-url)))
    (add-handler ms-site subpath (cl-naive-session::make-webserver-handler transport))
    transport))

(defvar *server-transport* nil)

(defun microservice-/ms/test ()
  (let* ((*server-transport*  (create-microservice-transport-server *server* "/ms" "/test"))
         (session             (make-instance 'cl-naive-session::server-session)))
    (cl-naive-session::register-session *server-transport* session)
    (unwind-protect
         (loop
           :for counter :from 1
           :for request := (cl-naive-session::receive-packet session)
           :for response := (make-instance
                             'packet
                             :message (list :received counter
                                            :request (packet-message request)
                                            :parameters (packet-parameters request))
                             :parameters '(:type :response))
           :do (format t "~A Received packet: ~S ~S~%" 'microservice-/ms/test
                       (packet-parameters request)
                       (packet-message request))
               (format t "~A Sending  packet: ~S ~S~%" 'microservice-/ms/test
                       (packet-parameters response)
                       (packet-message response))
               (finish-output)
               (cl-naive-session::send-packet session response)
           :until (eql request :stop))
      (cl-naive-session::unregister-session *server-transport* session)
      (remove-handler (get-site "/ms") "/test"))))

