(defpackage :cl-naive-session.tests.transport.client
  (:use :common-lisp
        :cl-naive-session
        :cl-naive-session.tests.transport.global)
  (:export :start :stop :run-client-request))
(in-package :cl-naive-session.tests.transport.client)

(defvar *session* nil)

(defun start ()
  (unless *session*
    (setf *session* (open-session (format nil "http://localhost:~A/ms/test" *port*)))))

(defun stop ()
  (when *session*
    (format t "session state = ~S~%" (session-state *session*))
    (unless (eq :deleted (session-state *session*))
      (close-session *session*))
    (setf *session* nil)))

(defun run-client-request ()
  (format *trace-output* "1 session state = ~S~%" (session-state *session*))
  (handler-case
      (send *session* (make-instance 'packet
                                     :parameters '(:message-kind "test")
                                     :message '(:test :test :test)))
    (error (err)
      (format *trace-output* "Error while sending message:   ~A~%" err)
      (format *trace-output* "1.e session state = ~S~%" (session-state *session*))
      (force-output *trace-output*)
      (return-from run-client-request nil)))
  (format *trace-output* "2 session state = ~S~%" (session-state *session*))
  (force-output *trace-output*)
  (prog1
      #-(and)
      (handler-bind
          ((error (lambda (err)
                    (format *trace-output* "~&Error while receiving message: ~A~%" err)
                    (format *trace-output* "2.e session state = ~S~%" (session-state *session*))
                    (force-output *trace-output*)
                    (invoke-debugger err)
                    (return-from run-client-request nil))))
        (receive *session*))
      (handler-case
          (receive *session*)
        (error (err)
          (format *trace-output* "Error while receiving message: ~A~%" err)
          (format *trace-output* "2.e session state = ~S~%" (session-state *session*))
          (force-output *trace-output*)
          (return-from run-client-request nil)))

      (format *trace-output* "3 session state = ~S~%" (session-state *session*))
      (force-output *trace-output*)))
