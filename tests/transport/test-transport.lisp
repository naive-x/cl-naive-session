(defpackage :cl-naive-session.tests.transport.unit-tests
  (:use :cl :cl-naive-tests :cl-naive-session)
  (:export))
(in-package :cl-naive-session.tests.transport.unit-tests)

;; Define here transport unit tests.


(defun has-parameters-p (expected actual)
  (let ((expected-parameters (car expected))
        (actual-parameters (car actual)))
    (loop :for (parameter expected-value) :on expected-parameters :by (function cddr)
	  :for actual-value := (getf actual-parameters  parameter)
	  :always (equalp expected-value actual-value))))

(defun setup ()
  (cl-naive-session.tests.transport.run:start))

(defun teardown ()
  (cl-naive-session.tests.transport.run:stop))

(defmacro with-test-environment (&body body)
  `(unwind-protect
        (progn (setup)
               ,@body)
     (teardown)))

(defclass test-transport (cl-naive-session::multiple-sessions-mixin cl-naive-session::transport)
  ((lock :initform (bt:make-lock) :reader cl-naive-session::transport-lock)))


(testsuite  :transport

  (testcase :get-session-by-id/null/no-listening-sessionn
            :expected NIL
            :equal 'equalp
            :actual (let ((transport (make-instance 'test-transport)))
		              (cl-naive-session::get-session-by-id transport nil))
            :info  "CL-NAIVE-SESSION::GET-SESSION-BY-ID should return NIL when no accepting session exist.")

  (testcase :get-session-by-id/null/accepting-session
            :expected 'got-accepting-session
            :equal 'eql
            :actual (let* ((transport (make-instance 'test-transport))
                           (session   (make-instance 'session :transport transport)))
                      (cl-naive-session::register-session transport session)
                      (when (eq session (cl-naive-session::get-session-by-id transport nil))
                        'got-accepting-session))
            :info "CL-NAIVE-SESSION::GET-SESSION-BY-ID should return the next accepting session when it exists.")

  (testcase :get-session-by-id/null/accepting-sessions
            :expected 'got-accepting-sessions
            :equal 'eql
            :actual (let* ((transport (make-instance 'test-transport))
                           (session1  (make-instance 'session :transport transport))
                           (session2  (make-instance 'session :transport transport)))
                      (cl-naive-session::register-session transport session1)
                      (cl-naive-session::register-session transport session2)
                      (let ((result1 (cl-naive-session::get-session-by-id transport nil))
                            (result2 (cl-naive-session::get-session-by-id transport nil))
                            (result3 (cl-naive-session::get-session-by-id transport nil)))
                        (if (and (eq session1 result1)
                                 (eq session2 result2)
                                 (eq nil      result3))
                            'got-accepting-sessions
                            `(and (eq ,session1 ,result1)
                                  (eq ,session2 ,result2)
                                  (eq ,nil      ,result3)))))
            :info "CL-NAIVE-SESSION::GET-SESSION-BY-ID should return the next accepting sessions when the exist.")

  (testcase :get-session-by-id/null/not-unregistered-session
            :expected 'got-accepting-sessions
            :equal 'eql
            :actual (let* ((transport (make-instance 'test-transport))
                           (session1  (make-instance 'session :transport transport))
                           (session2  (make-instance 'session :transport transport))
                           (session3  (make-instance 'session :transport transport)))
                      (cl-naive-session::register-session transport session1)
                      (cl-naive-session::register-session transport session2)
                      (cl-naive-session::register-session transport session3)
                      (cl-naive-session::unregister-session transport session2)
                      (let ((result1 (cl-naive-session::get-session-by-id transport nil))
                            (result2 (cl-naive-session::get-session-by-id transport nil))
                            (result3 (cl-naive-session::get-session-by-id transport nil)))
                        (if (and (eq session1 result1)
                                 (eq session3 result2)
                                 (eq nil      result3))
                            'got-accepting-sessions
                            `((and (eq ,session1 ,result1)
                                   (eq ,session3 ,result2)
                                   (eq ,nil      ,result3))
                              :session2 ,session2))))
            :info "CL-NAIVE-SESSION::GET-SESSION-BY-ID should return the next not unregisted accepting sessions.")


  (testcase :integration-http-drakma-webserver
            :expected '((:STATUS 404 :CHARSET "UTF-8" :S-VERSION 1 :S-COMMAND "response" :S-STATUS "reject") . NIL)
            :equal 'has-parameters-p
            :actual (with-test-environment
                      (let ((packet ;; (cl-naive-session.tests.transport.client:run-client-request)
                              (cl-naive-session.tests.transport.run:run)))
                        

                        (and packet
                             (cons (packet-parameters packet)
                                   (packet-message packet)))))
            :info "Integration test using the http-drakma-transport and http-webserver-transport."
            :disabled t)

  ) ;; testsuite transport
