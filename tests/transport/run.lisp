(defpackage :cl-naive-session.tests.transport.run
  (:use :common-lisp
   :cl-naive-session.tests.transport.global)
  (:export :start :stop :run)
  (:documentation "

Usage:

    (in-package :cl-naive-session.tests.transport.run)
    (start)
    (run)
    (run) ; …
    (stop)

"))
(in-package :cl-naive-session.tests.transport.run)


(defun get-resource (uri-string)
  (multiple-value-bind (out status headers uri stream closep message)
      (drakma:http-request uri-string)
    (declare (ignore headers uri message)) (when closep (close stream))
    (list status out)))

(defvar *microservice-thread* nil)

(defun start-microservice ()
  (unless *microservice-thread*
    (setf *microservice-thread*
          (bt:make-thread (lambda ()
                            (cl-naive-session.tests.transport.microservice::microservice-/ms/test))
                          :name "microservices.tests.session/ms/test"
                          :initial-bindings `((*trace-output* . ,*trace-output*)
                                              (*error-output* . ,*error-output*)
                                              (*standard-output* . ,*standard-output*)
	                                      (*print-right-margin* . nil))))))

(defun stop-microservice ()
  (when *microservice-thread*
    (bt:destroy-thread *microservice-thread*)
    (setf *microservice-thread* nil)))

(defun stop ()
  (cl-naive-session.tests.transport.client:stop)
  (stop-microservice)
  (cl-naive-session.tests.transport.webserver::stop-server 
   cl-naive-session.tests.transport.webserver::*server*)
  (cl-naive-webserver:deregister-server cl-naive-session.tests.transport.webserver::*server*))

(defun start ()
  (handler-case
      (cl-naive-session.tests.transport.webserver:main)
    (usocket:address-in-use-error (err)
      (format *error-output* "Error while starting the server: ~A~%" err)
      (return-from start)))
  (assert (equalp (get-resource (format nil "http://localhost:~A/test/home" *port*))
                  '(200 "Yeeeha she is alive and kicking!")))
  (start-microservice)
  ;; The site is created in a parallel thread, so wait for it:
  (loop :until (cl-naive-webserver:get-site "/ms") :do (sleep 1))
  ;; (cl-naive-session.tests.transport.global::get-handler "/ms" "/test")
  (trace-handler "/ms" "/test")
  (setf cl-naive-webserver:*debug* t
        cl-naive-tests:*debug* t
        cl-naive-log:*minimal-log-level* :debug)
  (cl-naive-session.tests.transport.client:start))

(defun run ()
  (pushnew `(*trace-output* . ,*standard-output*) bt:*DEFAULT-SPECIAL-BINDINGS* :test (function equalp))
  (pushnew `(*error-output* . ,*standard-output*) bt:*DEFAULT-SPECIAL-BINDINGS* :test (function equalp))
  (print (cl-naive-session.tests.transport.client:run-client-request))
  (sleep 1)
  (unless (eq :deleted (cl-naive-session.tests.transport.client::session-state
                        cl-naive-session.tests.transport.client::*session*))
    (print (cl-naive-session.tests.transport.client:run-client-request))
    (sleep 1)))


;; CL-NAIVE-WEBSERVER:HANDLE-RESOURCE-REQUEST
;; cl-naive-webserver:register-site
#-(and)
(maphash (lambda (k v)
           (declare (ignore v))
           (print k)) 
         cl-naive-webserver::*sites*)
