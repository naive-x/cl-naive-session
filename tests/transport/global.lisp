(defpackage :cl-naive-session.tests.transport.global
  (:use :common-lisp)
  (:export
   :*port*
   :tracing-handler
   :trace-handler))
(in-package :cl-naive-session.tests.transport.global)

(defvar *port* 33036)

(defun get-handler (site subpath)
  (gethash subpath
           (cl-naive-webserver:handlers
            (cl-naive-webserver:get-site site))))

(defun tracing-handler (site subpath)
  (let ((handler (get-handler site subpath)))
    (lambda (script-name)
      (format t "~&Will call handler for ~S~%" script-name)
      (finish-output)
      (unwind-protect
           (funcall handler script-name)
        (format t "~&Did call handler for ~S~%" script-name)
        (finish-output)))))

(defun trace-handler (site subpath)
  (setf (gethash subpath
                 (cl-naive-webserver:handlers
                  (cl-naive-webserver:get-site site)))
        (tracing-handler site subpath)))
