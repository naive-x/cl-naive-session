(defpackage :cl-naive-session.tests.packet.unit-tests
  (:use
   :common-lisp
   :cl-naive-tests
   :cl-naive-session
   :cl-naive-session.tests.common)
  (:export))
(in-package :cl-naive-session.tests.packet.unit-tests)

;; Define here packet unit tests.


;; :packet :packet-parameters :packet-message
;; :encode-packet :decode-packet


(testsuite :packets
  
  (testcase :encode-packet/http
            :info "Encode a packet for an HTTP transport."
	    :equal 'result-equalp
            :expected '((("CHARSET" . "UTF-8")
                         ("CONTENT-TYPE" . "application/x-lisp")
                         ("MESSAGE-TYPE" . "simple-message-type")
                         ("VERSION" . "1"))
		        . #(40 58 77 69 83 83 65 71 69 45 84 89 80 69
		            32 34 115 105 109 112 108 101 45 109 101 115
		            115 97 103 101 45 116 121 112 101 34 32 58 82
		            69 81 85 69 83 84 32 58 68 79 45 83 79 77 69
		            84 72 73 78 71 32 58 80 65 82 65 77 69 84 69
		            82 83 32 40 49 50 51 32 34 104 101 108 108 111
		            34 41 41))
            :actual (let ((transport (make-instance 'cl-naive-session::http-drakma-transport))
                          (packet   (make-instance 'packet
                                                   :parameters '(:content-type "application/x-lisp"
                                                                 :message-type "simple-message-type"
                                                                 :version 1)
                                                   :message '(:message-type "simple-message-type"
                                                              :request :do-something
                                                              :parameters (123 "hello")))))
                      (cl-naive-session::encode-packet transport packet)))

  (testcase :decode-packet/http
            :info "Decode a packet from an HTTP transport."
	    :equal 'result-equalp
            :expected '((:content-type "application/x-lisp"
                         :message-type "simple-message-type"
                         :version 1
                         :charset "UTF-8")
                        . (:message-type "simple-message-type"
                           :request :do-something
                           :parameters (123 "hello")))
            
            :actual (let* ((transport (make-instance 'cl-naive-session::http-drakma-transport))
                           (packet
                             (cl-naive-session::decode-packet
                              transport
                              (cons '(("CHARSET" . "UTF-8")
                                      ("CONTENT-TYPE" . "application/x-lisp")
                                      ("MESSAGE-TYPE" . "simple-message-type")
                                      ("VERSION" . "1"))
                                    (coerce
                                     #(40 58 77 69 83 83 65 71 69 45 84 89 80 69
                                       32 34 115 105 109 112 108 101 45 109 101 115
                                       115 97 103 101 45 116 121 112 101 34 32 58 82
                                       69 81 85 69 83 84 32 58 68 79 45 83 79 77 69
                                       84 72 73 78 71 32 58 80 65 82 65 77 69 84 69
                                       82 83 32 40 49 50 51 32 34 104 101 108 108 111
                                       34 41 41) 
                                     'cl-naive-session::octet-vector)))))
                      (and (typep packet 'packet)
                           (cons (packet-parameters packet)
                                 (packet-message packet)))))

  (testcase :encode-packet/stream
            :info "Encode a packet for a stream transport."
	    :equal 'equalp
            :expected #(40 40 58 67 79 78 84 69 78 84 45 84 89 80 69 32 34 97
                        112 112 108 105 99 97 116 105 111 110 47 120 45 108 105
                        115 112 34 32 58 77 69 83 83 65 71 69 45 84 89 80 69 32
                        34 115 105 109 112 108 101 45 109 101 115 115 97 103 101
                        45 116 121 112 101 34 32 58 86 69 82 83 73 79 78 32 49
                        41 32 58 77 69 83 83 65 71 69 45 84 89 80 69 32 34 115
                        105 109 112 108 101 45 109 101 115 115 97 103 101 45 116
                        121 112 101 34 32 58 82 69 81 85 69 83 84 32 58 68 79 45
                        83 79 77 69 84 72 73 78 71 32 58 80 65 82 65 77 69 84 69
                        82 83 32 40 49 50 51 32 34 104 101 108 108 111 34 41 41)
            :actual (let ((transport (make-instance 'cl-naive-session::stream-transport))
                          (packet   (make-instance 'packet
                                                   :parameters '(:content-type "application/x-lisp"
                                                                 :message-type "simple-message-type"
                                                                 :version 1)
                                                   :message '(:message-type "simple-message-type"
                                                              :request :do-something
                                                              :parameters (123 "hello")))))
                      (cl-naive-session::encode-packet transport packet)))

  (testcase :decode-packet/stream
            :info "Decode a packet from a stream transport."
	    :equal 'result-equalp
            :expected '((:content-type "application/x-lisp"
                         :message-type "simple-message-type"
                         :version 1)
                        . (:message-type "simple-message-type"
                           :request :do-something
                           :parameters (123 "hello")))
            
            :actual (let ((transport (make-instance 'cl-naive-session::stream-transport)))
                      (let ((packet
                              (cl-naive-session::decode-packet
                               transport
                               (coerce
                                #(40 40 58 67 79 78 84 69 78 84 45 84 89 80 69 32 34 97
                                  112 112 108 105 99 97 116 105 111 110 47 120 45 108 105
                                  115 112 34 32 58 77 69 83 83 65 71 69 45 84 89 80 69 32
                                  34 115 105 109 112 108 101 45 109 101 115 115 97 103 101
                                  45 116 121 112 101 34 32 58 86 69 82 83 73 79 78 32 49
                                  41 32 58 77 69 83 83 65 71 69 45 84 89 80 69 32 34 115
                                  105 109 112 108 101 45 109 101 115 115 97 103 101 45 116
                                  121 112 101 34 32 58 82 69 81 85 69 83 84 32 58 68 79 45
                                  83 79 77 69 84 72 73 78 71 32 58 80 65 82 65 77 69 84 69
                                  82 83 32 40 49 50 51 32 34 104 101 108 108 111 34 41 41) 
                                'cl-naive-session::octet-vector))))
                        (and (typep packet 'packet)
                             (cons (packet-parameters packet)
                                   (packet-message packet))))))

  
  (testcase :merge-packets
            :info "Merge packets."
	    :equal 'result-equalp
            :expected '((:header-a-1 "a123"
                         :header-a-2 "a234"
                         :header-c-1 "c123"
                         :header-c-2 "c234"
                         :header-b-1 "b123"
                         :header-b-2 "b234")
                        . ((:message-b-1 "foo bar baz")
                           (:message-b-2 "quux")))
            
            :actual (let ((packet (merge-packets (make-instance 'packet
                                                                :parameters '(:header-a-1 "a123"
                                                                              :header-a-2 "a234")
                                                                :message '(:message-a-1 "babar"))
                                                 (make-instance 'packet
                                                                :parameters '(:header-b-1 "b123"
                                                                              :header-b-2 "b234")
                                                                :message '((:message-b-1 "foo bar baz")
                                                                           (:message-b-2 "quux")))
                                                 '(:header-c-1 "c123"
                                                   :header-c-2 "c234"))))
                      (and (typep packet 'packet)
                           (cons (packet-parameters packet)
                                 (packet-message packet)))))

  ) ;; testsuite :packets

