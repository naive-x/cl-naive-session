(defsystem "cl-naive-session.tests"
  :description "Tests for cl-naive-session."
  :version "2023.7.26"
  :author "Phil Marneweck <phil@psychedelic.co.za>"
  :licence "MIT"
  :depends-on (:cl-naive-tests
               ;; packet, transport and session:
               :cl-naive-session
               :cl-naive-webserver.hunchentoot
               :drakma
               :bordeaux-threads)

  
  :components ((:module "tests/common"
                :components ((:file "common")))
               
               (:module "tests/packet"
                :depends-on ("tests/common")
                :components ((:file "test-packet")))
               
               (:module "tests/transport"
                :depends-on ("tests/common")
                :components ((:file "global")
                             (:file "webserver"    :depends-on ("global"))
                             (:file "microservice" :depends-on ("webserver"))
                             (:file "client"       :depends-on ("global"))
                             (:file "run"          :depends-on ("global"
                                                                "webserver"
                                                                "microservice"
                                                                "client"))
                             (:file "test-transport" :depends-on ("run"))))

               (:module "tests/session"
                :components ((:file "test-client-session")
                             (:file "test-server-session")
                             (:file "global")
                             (:file "webserver"    :depends-on ("global"))
                             (:file "microservice" :depends-on ("webserver"))
                             (:file "client"       :depends-on ("global"))
                             (:file "run"          :depends-on ("global"
                                                                "webserver"
                                                                "microservice"
                                                                "client"))))))

