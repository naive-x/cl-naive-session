(defsystem "cl-naive-session.websockets"
  :description "Session layer over websocket."
  :version "2023.10.24"
  :author "Phil Marneweck <phil@psychedelic.co.za>"
  :licence "MIT"
  :depends-on (:cl-naive-session
	       :cl-naive-websockets
               :cl-naive-websockets.client.drakma
               :cl-naive-websockets.server.hunchentoot)
  :components ((:file "src/transport-websockets")))

