(in-package :cl-naive-session)

#|

Session messages are s-expressions that are printable readably.

Notably, the symbols they contain are interned in whatever package the
remote side reads them in, so better use keywords in general.

Converted to strings, then encoded into UTF-8 octet vectors, they
constitute the payload of a packet.


Along with those messages, we also transport parameters, which are
p-lists of strings.  Those parameters may be transported thru HTTP
headers, or just along with the payload, as packet parameters.

PACKET objects contain a message, and a p-list of parameters.


(encode-packet transport message &optional parameters)
and decode-packett
|#

;;; Implementation for HTTP transport:

(defmethod print-object ((packet packet) stream)
  (print-unreadable-object (packet stream :type t :identity t)
    (with-io-syntax
      (let ((*print-readably* nil))
        (format stream "(~S . ~S)"
                (packet-parameters packet)
                (packet-message packet)))))
  packet)


(defmethod encode-packet ((transport http-transport) (packet packet))
  "
For HTTP transports, ENCODE-PACKET returns a CONS cell whose CAR is an
a-list of headers and whose CDR is the payload an UTF-8 octet-vector.
The headers is an a-list such as: 
    ((:content-type . \"application/x-lisp\")
     (:content-length . 1234) 
     (:charset . :utf-8))
"
  ;; (packet-message packet) is a s-expression
  ;; 1- print it readably,
  ;; 2- encode the string in utf-8 octets
  
  (cons (prepare-parameters (list* :charset :utf-8 (packet-parameters packet)))
	(babel:string-to-octets 
         (with-io-syntax
           (let ((*print-readably* nil))
            (prin1-to-string (prepare-payload (packet-message packet)))))
         :encoding :utf-8)))


(defmethod decode-packet ((transport http-transport) (packet-data cons))
  "
For HTTP transports, PACKET-DATA is a cons cell containing the request
headers a-list in the car, and an octet-vector containing the UTF-8
bytes of the message.  DECODE-PACKET returns a PACKET instance built
from the request headers found in the transport, and the message read
from the packet-data. 
"
  (make-instance 'packet
                 :parameters (parse-parameters (car packet-data))
                 :message    (with-io-syntax
                               (read-from-string (babel:octets-to-string
                                                  (cdr packet-data)
                                                  :encoding :utf-8)))))

;;; Implementation for stream transport:

(defmethod encode-packet ((transport stream-transport) (packet packet))
  "
For stream transports, ENCODE-PACKET returns an UTF-8 encoded octet vector
containing the s-expression to be sent, wrapped in the cdr of a cons cell,
whose car contains the parameters of the packet.
"
  (babel:string-to-octets 
   (with-io-syntax
     (prin1-to-string 
      (cons (packet-parameters packet)
            (packet-message packet))))
   :encoding :utf-8))


(defmethod decode-packet ((transport stream-transport) (packet-data vector))
  "
For stream transports, DECODE-PACKET returns a PACKET instance
built from the payload found in the packet. 

For this method, the payload is an UTF-8 octet-vector.
"
  (let ((data (with-io-syntax
                (read-from-string
                 (babel:octets-to-string packet-data :encoding :utf-8)))))
    (make-instance 'packet
                   :parameters (car data)
                   :message    (cdr data))))

;;;; THE END ;;;;
