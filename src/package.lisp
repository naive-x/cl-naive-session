(in-package :common-lisp-user)

(defpackage :cl-naive-session
  (:use
   :cl
   :puri
   :cl-naive-log
   :cl-naive-websockets.wrapper)
  (:export

   ;; for cl-naive-webserver
   :create-server-transport
   :make-webserver-handler
   
   ;; Session:
   :list-all-sessions
   :session :server-session :client-session
   :session-id :session-state :session-alive-p
   :session-local-url
   :session-remote-url

   :accept-session :open-session
   :close-session
   
   :receive
   :send
   
   ;; Message:
   :packet :packet-parameters :packet-message
   :merge-packets
   :octet-vector
   :encode-packet
   :decode-packet

   ;; Transport & internal session stuff:
   :get-session-by-id
   :unregister-session
   :register-session
   :transport
   :state
   :transport-secure
   :http-transport
   :transport-lock
   :http-drakma-transport
   :remote-url
   :remote-stream
   :request-queue
   :response-queue
   :http-webserver-transport
   :webserver
   :local-url
   :stream-transport
   :websockets-drakma-transport
   :websockets-endpoint
   :websockets-webserver-transport
   :ssl-client-transport
   :ssl-server-transport
   :request-mailbox
   :response-mailbox
   :session-version
   :session-command
   :session-status
   :session-message))
