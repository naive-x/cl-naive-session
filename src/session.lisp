(in-package :cl-naive-session)


;;;
;;; Sessions
;;;


(defvar *sessions* (make-hash-table))

(defmethod register ((session session))
  (setf (gethash (session-id session) *sessions*) session))

(defmethod unregister ((session session))
  (remhash (session-id session) *sessions*))

(defun list-all-sessions ()
  (let ((result (list)))
    (maphash (lambda (key value)
               (declare (ignore key))
               (push value result))
             *sessions*)))


(defun session-alive-p (session)
  (not (eq :deleted (state session))))

(defmethod (setf session-id) (new-id (session session))
  (remhash (session-id session) *sessions*)
  (setf (slot-value session 'id) new-id)
  (setf (gethash (session-id session) *sessions*) session)
  new-id)

(defmacro transition (session new-state)
  (let ((vsession (gensym)))
    `(let ((,vsession ,session))
       (setf (state ,vsession) ,new-state))))

;;;
;;; Messages
;;;


(defmethod merge-packets ((session-packet packet) (user-packet packet) &optional additionnal-parameters)
  (make-instance 'packet
                 :parameters (append (packet-parameters session-packet)
                                     additionnal-parameters
                                     (packet-parameters user-packet))
                 :message (packet-message user-packet)))

(defun prefixp (prefix string)
  (and (<= (length prefix) (length string))
       (string-equal prefix (subseq string 0 (length prefix)))))
                     
(defun session-parameter-p (parameter)
  (prefixp "S-" (string parameter)))

(defmethod remove-session-parameters ((session-packet packet))
  #-(and)
  (make-instance 'packet
                 :parameters (loop :for (indicator value) :on (packet-parameters session-packet)
                                   :by (function cddr)
                                   :unless (session-parameter-p indicator)
                                   :collect indicator :and :collect value)
                 :message (packet-message session-packet))
  session-packet)



(defmethod session-request-create ((session session))
  (make-instance 'packet :parameters (list :s-version 1
                                           :s-id (session-id session)
                                           :s-command "create")))

(defmethod session-request-delete ((session session))
  (make-instance 'packet :parameters (list :s-version 1
                                           :s-id (session-id session)
                                           :s-command "delete")))

(defmethod session-request-message ((session session))
  (make-instance 'packet :parameters (list :s-version 1
                                           :s-id (session-id session)
                                           :s-command "request")))

(defmethod session-response-ok ((session session))
  (make-instance 'packet :parameters (list :s-version 1
                                           :s-id (session-id session)
                                           :s-command "response"
                                           :s-status "ok")))


(defmethod session-response-reject ((session-id t) &optional (message "Session ID not found."))
  (make-instance 'packet :parameters (list :s-version 1
                                           :s-id session-id
                                           :s-command "response"
                                           :s-status "reject"
                                           :s-message message)))



(defmethod session-version ((packet packet))
  (let ((version (getf (packet-parameters packet) :s-version)))
    (if version
        (handler-case (parse-integer version)
          (parse-error ()
            (error 'invalid-session-version :packet packet)))
        (error 'missing-session-version :packet packet))))
(defmethod session-id ((packet packet))
  (or (getf (packet-parameters packet) :s-id)
      (error 'missing-session-id :packet packet)))
(defmethod session-command ((packet packet))
  (or (getf (packet-parameters packet) :s-command)
      (error 'missing-session-command :packet packet)))
(defmethod session-status ((packet packet))
  (or (getf (packet-parameters packet) :s-status)
      (error 'missing-session-status :packet packet)))
(defmethod session-message ((packet packet))
  (or (getf (packet-parameters packet) :s-message)
      (error 'missing-session-message :packet packet)))


;;;
;;; Session
;;;


#|
** Session Layer

=accept-session= starts a server on =local-URL=, listening, and
accepting a new connection, that will create a new session.  The
session is returned.  Or, the server may already be running (eg. a
hunchentoot or webserver), and =accept-session= will install the
handler onto the running server to receive the requests.

Note: For transports that use handlers such as webserver or
websockets, =accept-session= will work lazily:

- it creates a session object;

- the underlying transport installs a handler;

- we must now wait for the client to create a session and connect, so:

- we return immediately, and let the client:

- call =receive=.

In this early state, the session still needs to be established:

- =receive= will call the transport =receive-packet=;

- when the underlying server receives an incoming connection and a
  request to create a new session, THEN this session creation request
  will be hooked with this stack, and =receive-packet= will return the
  session creation request.

- it is then processed by the session layer, to finalize the session
  creation.  Since we have independent creation of session objects on
  both side, the session ID may be different and need to be mapped
  between the client and the server. Or we may use provisionnal
  server-session-id until the session is created on the client side,
  and then change the session id to client-session-id.

- the session layer accepts the session with a =send-packet=, 
  and calls =receive-packet= to receive the first message.  NOTE: we
  may use only parameters for session layer traffic, so the client may
  send the first message along with the session creation traffic.

- the client sends at last the first message, and we return it from
  the server =receive= call.





NOTE: we don't want to actually start the web server, because the
system may have multiple microservices hooked to the same web server,
and the system may include other web services or sites.  But we may
install a site and a handler for the microservice.  (and remove it
with delete-session).

A session is created when the client calls =open-session=.  The client
will then send a message to the server using the transport layer, and
the server will be able to create the session server-side.

A session is made of a sequence of several connections (transport
layer).  There must be a way to connect the connections (to identify
which session they belong to).



=open-session= creates a new session with the server at =remote-URL=.
Eventually, a connection will be created, session negociated, and data
exchange will start.



|#

;; (accept-session local-URL) -> session
;; (open-session remote-URL) -> session
;; (close-session session)
;; (session-id session) -> ID
;; (session-remote-url session) -> URL
;; (session-local-url session) -> URL
;; (session-status session) -> status
;; 
;; (list-all-sessions) -> list of session
;; 
;; (send session payload [parameters])
;; (send session packet)
;; (receive session) -> packet






;;;
;;; Server Session
;;;

(defvar *microservice-id* (format nil "~A-~X" (machine-instance) (get-universal-time)))
(defvar *session-counter* 0)
(defun generate-new-session-id ()
  (format nil "~A-~X" *microservice-id* (incf *session-counter*)))

(defun accept-session (local-url &rest args &key &allow-other-keys)
  (let ((session (make-instance 'server-session
                                :local-url local-url
                                :transport (apply (function create-server-transport)
                                                  local-url args))))
    (transition session :s-virgin)
    (register session)
    (register-session (transport session) session)
    session))


;;;
;;; Client Session
;;;

(defun open-session (remote-url &rest args &key &allow-other-keys)
  (let ((session (make-instance 'client-session
                                :id (generate-new-session-id)
                                :remote-url remote-url
                                :transport (apply (function create-client-transport)
                                                  remote-url args))))
    (transition session :c-virgin)
    (register session)
    (register-session (transport session) session)
    session))


#|

receive-packet will receive a packet, from the transport layer, either:

- a drakma response, in which case we may have several drakma requests
  in parallel, and several client-session waiting each for its own
  response. Since it's done in parallel in different threads, we need
  to respond to the right one.

- a webserver request, in which case we may have several
  server-session waiting for messages, each in a different state.
  The message contains a session-id and a session-command that allows us
  to dispatch the request to the right server-session.

- a websocket (stream) message, in which case we should have a
  reference to the right session endpoint. (TO BE DONE).


Currently, the packets are posted in handle-request with:
#+begin_src lisp
(mailbox-post (request-mailbox transport) packet)
#+end_srcc

But we need to do some dispatching there in handle-request.

(session-id packet)
(session-command packet)

make-webserver-handler produces a single handler for a given URL.
We may have several sessions hanging on the same URL, so we need to
dispatch the request to the right session from a single transport instance.

The http-webserver-transport instance needs to have a table of
server-sessions to dispatch to.

|#


(defmethod receive ((session session))
  (receive-packet session))

(defmethod receive ((session server-session))
  (case (state session)
    (:s-virgin
     (let ((message (call-next-method)))
       (if (equalp "create" (session-command message))
           (progn
             (setf (session-id session) (concatenate 'string (generate-new-session-id) "=" (session-id message)))
             (register-session (transport session) session)
             (transition session :s-opening)
             (remove-session-parameters message))
           (error 'invalid-session-command
                  :packet message
                  :got-command (session-command message)
                  :expected-comamnd "create"))))
    ((:s-opening :s-idle)
     (error 'session-has-wrong-state-for-transition
            :session session
            :side :server
            :transition "receive a message"
            :current-state (state session)))
    (:s-waiting
     (let ((message (call-next-method)))
       (if (equalp "delete" (session-command message))
           (progn
             (send-packet session (session-response-ok session))
             (delete-session session))
           (transition session :s-idle))
       (remove-session-parameters message)))
    ;; (:s-closing
    ;;  (let ((message (call-next-method)))
    ;;    (send-packet session (session-request-delete session))
    ;;    (delete-session session)
    ;;    (remove-session-parameters message)))
    (otherwise
     (error 'session-has-wrong-state-for-transition
            :session session
            :side :server
            :transition "receive a message"
            :current-state (state session)))))

(defmethod receive ((session client-session))
  (ecase (state session)
    ((:c-virgin :c-idle)
     (error 'session-has-wrong-state-for-transition
            :session session
            :side :client
            :transition "receive a message"
            :current-state (state session)))
    (:c-opening
     (let ((message (call-next-method)))
       (cond
         ((equalp "ok" (session-status message))
          (setf (session-id session) (session-id message))
          (transition session :c-idle)
          (remove-session-parameters message))
         ((equalp "reject" (session-status message))
          (delete-session session)
          (error 'invalid-session-status
                 :session session
                 :packet message
                 :got-status (session-status message)
                 :expected-status "ok"))
         (t
          (error 'invalid-session-status
                 :session session
                 :packet message
                 :got-status (session-status message)
                 :expected-status "ok")))))
    (:c-waiting
     (let ((message (call-next-method)))
       (if (equalp "delete" (session-command message))
           (delete-session session)
           (transition session :c-idle))
       (remove-session-parameters message)))
    (:c-closing
     (let ((message (call-next-method)))
       (if (equalp "ok" (session-status message))
           (delete-session session)
           (error 'invalid-session-status
                 :session session
                 :packet message
                 :got-status (session-status message)
                 :expected-status "ok"))
       (remove-session-parameters message))))) 

(defmethod send ((session session) (message t) &optional parameters)
  (let ((message (make-instance 'packet
                                :parameters parameters
                                :message message)))
    (send-packet session message))
  session)

(defmethod send ((session server-session) (message packet) &optional parameters)
  (case (state session)
    (:s-opening
     (let ((message (merge-packets (session-response-ok session)
                                   message parameters)))
       (send-packet session message)
       (transition session :s-waiting)))
    (:s-idle
     (let ((message (merge-packets (session-request-message session)
                                   message parameters)))
       (send-packet session message)
       (transition session :s-waiting)))
    (otherwise
     (error 'session-has-wrong-state-for-transition
            :session session
            :side :server
            :transition "send a message"
            :current-state (state session))))
  session)

(defmethod send ((session client-session) (message packet) &optional parameters)
  (case (state session)
    (:c-virgin
     (let ((message (merge-packets (session-request-create session)
                                   message parameters)))
       (send-packet session message)
       (transition session :c-opening)))
    (:c-idle
     (let ((message (merge-packets (session-request-message session)
                                   message parameters)))
       (send-packet session message)
       (transition session :c-waiting)))
    (otherwise
     (error 'session-has-wrong-state-for-transition
            :session session
            :side :client
            :transition "send a message"
            :current-state (state session))))
  session)
    
(defmethod close-session ((session server-session))
  (case (state session)
    (:deleted)
    (:s-idle
     (send-packet session (session-request-delete session))
     (delete-session session))
    (:s-waiting
     (transition session :s-closing)
     (receive-packet session) ; note: client packet is ignored.
     (send-packet session (session-request-delete session))
     (delete-session session))
    (otherwise
     (error 'session-has-wrong-state-for-transition
            :session session
            :side :server
            :transition "be closed"
            :current-state (state session))))
  session)

(defmethod close-session ((session client-session))
  (case (state session)
    (:deleted)
    (:c-virgin
     (transition session :deleted))
    (:c-idle
     (send-packet session (session-request-delete session))
     (transition session :c-closing)
     (receive session))
    (otherwise
     (error 'session-has-wrong-state-for-transition
            :session session
            :side :client
            :transition "be closed"
            :current-state (state session))))
  session)


(defmethod delete-session ((session session))
  (unregister session)
  (setf (transport session) nil)
  (transition session :deleted)
  session)

(defmethod delete-session ((session server-session))
  (unregister-session (transport session) session)
  (call-next-method))

;;;; THE END ;;;
