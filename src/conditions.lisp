(in-package :cl-naive-session)

(define-condition session-error (error)
  ((packet :initarg :packet :reader session-error-packet)))

(define-condition invalid-session-version (session-error)
  ()
  (:report (lambda (condition stream)
             (format stream "Invalid Session Version in packet ~S"
                     (session-error-packet condition)))))

(define-condition invalid-session-command (session-error)
  ((go-command :initarg :got-command :reader invalid-session-command-got-command)
   (expected-command :initarg :expected-command :reader invalid-session-command-expected-command))
  (:report (lambda (condition stream)
             (format stream "Invalid Session Command in packet ~S ; got ~S ; expected ~S"
                     (session-error-packet condition)
                     (invalid-session-command-got-command condition)
                     (invalid-session-command-expected-command condition)))))

(define-condition missing-session-version (session-error)
  ()
  (:report (lambda (condition stream)
             (format stream "Missing Session Version in packet ~S"
                     (session-error-packet condition)))))

(define-condition missing-session-id (session-error)
  ()
  (:report (lambda (condition stream)
             (format stream "Missing Session ID in packet ~S"
                     (session-error-packet condition)))))

(define-condition missing-session-command (session-error)
  ()
  (:report (lambda (condition stream)
             (format stream "Missing Session Command in packet ~S"
                     (session-error-packet condition)))))

(define-condition missing-session-status (session-error)
  ()
  (:report (lambda (condition stream)
             (format stream "Missing Session Status in packet ~S"
                     (session-error-packet condition)))))

(define-condition missing-session-message (session-error)
  ()
  (:report (lambda (condition stream)
             (format stream "Missing Session Message in packet ~S"
                     (session-error-packet condition)))))

(define-condition session-error-with-session (session-error)
  ((session :initarg :session :reader session-error-session)))

(define-condition session-has-wrong-state-for-transition (session-error-with-session)
  ((side :initarg :side :reader session-error-side)
   (current-state :initarg :current-state :reader session-error-current-state)
   (transition :initarg :transition :reader session-error-transition))
  (:report (lambda (condition stream)
             (format stream "~:(~A~) Session cannot ~A while in the ~S state"
                     (session-error-side condition)
                     (session-error-transition condition)
                     (session-error-current-state condition)))))

(define-condition invalid-session-status (session-error-with-session)
  ((got-status      :initarg :got-status      :reader invalid-session-status-got-status)
   (expected-status :initarg :expected-status :reader invalid-session-status-expected-status))
  (:report (lambda (condition stream)
             (format stream "Invalid Session Status in packet ~S ; Got ~S, expected ~S"
                     (session-error-packet condition)
                     (invalid-session-status-got-status condition)
                     (invalid-session-status-expected-status condition)))))
