(in-package :cl-naive-session)



(defmethod get-session-by-id ((transport multiple-sessions-mixin) session-id)
  (bt:with-lock-held ((transport-lock transport))
    (if session-id
        (gethash session-id (transport-sessions transport))
        (pop (gethash session-id (transport-sessions transport))))))

(defmethod register-session (transport session)
  (declare (ignore transport))
  session)

(defmethod unregister-session (transport session)
  (declare (ignore transport session))
  (values))

(defmethod register-session ((transport multiple-sessions-mixin) new-session)
  (bt:with-lock-held ((transport-lock transport))
    (let ((session-id (session-id new-session)))
      (if session-id
          (setf (gethash session-id (transport-sessions transport)) new-session)
          (setf (gethash session-id (transport-sessions transport))
                (nconc (gethash session-id (transport-sessions transport))
                       (list new-session))))))
  new-session)

(defmethod unregister-session ((transport multiple-sessions-mixin) session)
  (bt:with-lock-held ((transport-lock transport))
    (let ((session-id (session-id session)))
      (if session-id
          (remhash session-id (transport-sessions transport))
          (setf (gethash nil (transport-sessions transport))
                (remove session (gethash nil (transport-sessions transport)))))))
  (values))




(defmethod send-packet :around (channel packet)
  (declare (ignorable channel packet))
  ;; (cl-naive-webserver:log-message :debug "SEND-PACKET    ~A ~A" channel packet)
  (call-next-method))

(defmethod receive-packet :around (channel)
  (declare (ignorable channel))
  (let ((packet (call-next-method)))
    ;; (cl-naive-webserver:log-message :debug "RECEIVE-PACKET ~A ~A" channel packet)
    packet))

(defmethod send-packet ((session client-session) (packet packet))
  (send-packet (transport session) packet))

(defmethod receive-packet ((session client-session))
  (receive-packet (transport session)))

(defmethod send-packet ((session server-session) (packet packet))
  (mailbox-post (response-mailbox session) packet))

(defmethod receive-packet ((session server-session))
  (mailbox-collect (request-mailbox session)))

;;;
;;; http transports
;;;

(defmethod add-handler ((site cl-naive-webserver:site) (subpath string) (handler function))
  (setf (cl-naive-webserver:find-resource site subpath) handler))

(defmethod initialize-instance :after ((transport http-webserver-transport) &key site resource &allow-other-keys)
  (assert (typep site 'cl-naive-webserver:site) (site)
          "A ~S needs a webserver :site parameter, instead of ~S" 'http-webserver-transport site)
  (assert (stringp resource) (resource)
          "A ~S needs a string :resource parameter, instead of ~S" 'http-webserver-transport resource)
  (add-handler site resource (make-webserver-handler transport)))


(defmethod send-packet ((transport http-drakma-transport) (packet packet))
  ;; TODO: we should wait on a condition that (request-queue transport) is empty.
  (bt:with-lock-held ((transport-lock transport))
    (destructuring-bind (parameters . octets) (encode-packet transport packet)
      (multiple-value-bind (response status-code headers uri http-stream must-close status-text)
          (drakma:http-request (remote-url transport)
                               :method :post
                               :parameters parameters ; (alexandria:plist-alist parameters)
                               :content-type "application/octet-stream"
                               :content octets
                               :stream (remote-stream transport))

        ;; '(:protocol :method :force-ssl :certificate :key
        ;;   :certificate-password :verify :max-depth :ca-file
        ;;   :ca-directory :parameters :url-encoder :content :content-type
        ;;   :content-length :form-data :cookie-jar :basic-authorization
        ;;   :user-agent :accept :range :proxy :no-proxy-domains
        ;;   :proxy-basic-authorization :real-host :additional-headers
        ;;   :redirect :auto-referer :keep-alive :close
        ;;   :external-format-out :external-format-in :force-binary
        ;;   :want-stream :stream :preserve-uri :decode-content
        ;;   :connection-timeout :deadline)

        (declare (ignore uri))
        (if must-close
            (progn
              (close http-stream)
              (setf (remote-stream transport) nil))
            (setf (remote-stream transport) http-stream))
        (unless (= 200 status-code)
          (error "HTTP status ~A: ~A~% for request: ~S~%~S~% at ~S" status-code status-text 
                 parameters octets (remote-url transport)))
        (let ((packet (decode-packet transport (cons headers response))))
          (enqueue (response-queue transport) packet)
          packet)))))

(defmethod receive-packet ((transport http-drakma-transport))
  (dequeue (response-queue transport)))

(defmethod send-packet :around (channel packet)
  ;; (cl-naive-log:log-message :debug channel "SEND-PACKET    ~A ~A" channel packet)
  (call-next-method))

(defmethod receive-packet :around (channel)
  (let ((packet (call-next-method)))
    ;; (cl-naive-log:log-message :debug channel "RECEIVE-PACKET ~A ~A" channel packet)
    packet))

(defmethod send-packet ((session client-session) (packet packet))
  (send-packet (transport session) packet))

(defmethod receive-packet ((session client-session))
  (receive-packet (transport session)))

(defmethod send-packet ((session server-session) (packet packet))
  (mailbox-post (response-mailbox session) packet))

(defmethod receive-packet ((session server-session))
  (mailbox-collect (request-mailbox session)))

(defun read-payload (request)
  (getf request :post-data)
  #-(and)
  (let ((content-length (parse-integer (or (cdr (assoc :content-length (getf request :headers-in))) "0"))))
    (if (member (getf request :content-type)
                '("application/octet-stream")
                :test #'string-equal)
        (let ((octets (make-array content-length :element-type '(unsigned-byte 8))))
          (read-sequence octets (getf request :content-stream))
          octets)
        (let ((text (make-array content-length :element-type '(unsigned-byte 8))))
          (read-sequence text (getf request :content-stream))
          text))))

(defmethod handle-request ((transport http-webserver-transport)
                           webserver-request
                           script-name
                           request)
  (declare (ignore webserver-request script-name))
  ;; TODO: in case of error, we need to still mailbox-post/mailbox-collect.
  (let ((packet (decode-packet transport (cons (append (getf request :get-parameters)
                                                       (getf request :post-parameters))
                                               (read-payload request)))))
    (let ((session (get-session-by-id transport (session-id packet))))  
      (when (and (null session)
                 (equalp "create" (session-command packet)))
        (setf session (get-session-by-id transport nil)))
      (let* ((status 200)
             (response (if session
                           (progn
                             (mailbox-post (request-mailbox session) packet)
                             ;; wait for a response in response-mailbox
                             (mailbox-collect (response-mailbox session)))
                           (progn
                             ;; send a negative response
                             (setf status 404)
                             (session-response-reject (session-id packet)
                                                      (if (session-id packet)
                                                          "Session ID unknown."
                                                          "Missing a Session ID in headers."))))))
        (let* ((encoded (encode-packet transport response)))
          (setf (cl-naive-webserver:headers cl-naive-webserver:*reply*)
                (append `((:content-length . ,(length (cdr encoded)))
                          (:content-type . "application/octet-stream")
                          (:status . ,status))
                        (car encoded)
                        (cl-naive-webserver:headers cl-naive-webserver:*reply*)))
          (cdr encoded))))))


(defun make-webserver-handler (transport)
  (lambda (script-name)
    (handle-request transport cl-naive-webserver:*request* 
                    script-name
                    (list :headers-in      (hunchentoot:headers-in      hunchentoot:*request*)
                          :request-method  (hunchentoot:request-method  hunchentoot:*request*)
                          :request-uri     (hunchentoot:request-uri     hunchentoot:*request*)
                          :server-protocol (hunchentoot:server-protocol hunchentoot:*request*)
                          :local-addr      (hunchentoot:local-addr      hunchentoot:*request*)
                          :local-port      (hunchentoot:local-port      hunchentoot:*request*)
                          :remote-addr     (hunchentoot:remote-addr     hunchentoot:*request*)
                          :remote-port     (hunchentoot:remote-port     hunchentoot:*request*)
                          :content-stream  (hunchentoot::content-stream hunchentoot:*request*)
                          :post-data       (hunchentoot::get-post-data)
                          :cookies-in      (hunchentoot:cookies-in      hunchentoot:*request*)
                          :get-parameters  (hunchentoot:get-parameters  hunchentoot:*request*)
                          :post-parameters (hunchentoot:post-parameters hunchentoot:*request*)
                          :script-name     (hunchentoot:script-name     hunchentoot:*request*)
                          :query-string    (hunchentoot:query-string    hunchentoot:*request*)
                          :session         (hunchentoot:session         hunchentoot:*request*)
                          :aux-data        (hunchentoot::aux-data       hunchentoot:*request*)))))



;;;
;;; Transport creation
;;;

(defparameter *client-transports*
  '((:http  http-drakma-transport       nil)
    (:https http-drakma-transport       t))
  "This is an a-list mapping URI schemes to a list containing the
client transport class and a flag indicating whether the transport is
secure or not (ie. whether it uses SSL).")

(defun create-client-transport (remote-url &rest args &key  &allow-other-keys)
  (multiple-value-bind (class secure)
      (let ((entry (assoc (uri-scheme (parse-uri remote-url))
                          *client-transports*)))
        (unless entry
          (error "Unsupported scheme: ~A" (uri-scheme (parse-uri remote-url))))
        (values (second entry) (third entry)))
    (apply (function make-instance) class
           :remote-url remote-url
           :secure secure
           args)))

(defparameter *server-transports*
  '((:http  http-webserver-transport nil)
    (:https http-webserver-transport t))
  "This is an a-list mapping URI schemes to a list containing the
server transport class and a flag indicating whether the transport is
secure or not (ie. whether it uses SSL).")

(defvar *server-transport-instances* (make-hash-table :test (function equalp))
  "Maps local-url to server-transport instance.
When two sessions use the same local-url, they get the same transport,
and the transport instance dispatches to the rigth session.")

(defun create-server-transport (local-url &rest args &key &allow-other-keys)
  (multiple-value-bind (class secure)
      (let ((entry (assoc (uri-scheme (parse-uri local-url))
                          *server-transports*)))
        (unless entry
          (error "Unsupported scheme: ~A" (uri-scheme (parse-uri local-url))))
        (values (second entry) (third entry)))
    (let ((transport (gethash local-url *server-transport-instances*)))
      (if transport
          transport
          (let ((transport (apply (function make-instance) class
                                  :local-url local-url
                                  :secure secure
                                  :allow-other-keys t
                                  args)))
            (setf (gethash local-url *server-transport-instances*) transport)
            transport)))))


;;;; THE END ;;;;
