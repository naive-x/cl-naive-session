;;;; -*- mode:lisp;coding:utf-8 -*-
;;;;**************************************************************************
;;;;FILE:               mailbox.lisp
;;;;LANGUAGE:           Common-Lisp
;;;;SYSTEM:             Common-Lisp
;;;;USER-INTERFACE:     NONE
;;;;DESCRIPTION
;;;;
;;;;    MAILBOX: to send one message between two threads.
;;;;
;;;;    The consummer may call MAILBOX-COLLECT before or after the
;;;;    producer calls MAILBOX-POST.   If it calls before, then it
;;;;    waits until the producer notifies the mailbox is full.
;;;;
;;;;AUTHORS
;;;;    <PJB> Pascal J. Bourguignon <pjb@informatimago.com>
;;;;MODIFICATIONS
;;;;    2023-07-16 <PJB> Integrated into cl-naive-session.
;;;;    2014-04-27 <PJB> Extracted from process.
;;;;BUGS
;;;;LEGAL
;;;;    Copyright Pascal J. Bourguignon 2014 - 2023
;;;;**************************************************************************
(in-package :cl-naive-session)
(declaim (declaration stepper))

;; A mailbox is a place where a thread can post a message, and another
;; thread can collect it.  The mailbox is full when a message is
;; posted, and empty when it is collected.
(defstruct (mailbox
            (:conc-name %mailbox-)
            (:constructor %make-mailbox (lock condition)))
  lock condition message full)

(defun make-mailbox (&optional (name "mailbox"))
  "Return a new mailbox."
  (declare (stepper disable))
  (%make-mailbox (bt:make-lock name) (bt:make-condition-variable :name name)))

(defun mailbox-collect (mailbox)
  "Return the message posted in the MAILBOX, waiting if necessary."
  (declare (stepper disable))
  (let ((result nil))
    (bt:with-lock-held ((%mailbox-lock mailbox))
      (loop :until (%mailbox-full mailbox)
            :do (bt:condition-wait (%mailbox-condition mailbox) (%mailbox-lock mailbox)))
      (setf result (%mailbox-message mailbox)
            (%mailbox-message mailbox) nil
            (%mailbox-full mailbox) nil))
    result))

(defun mailbox-post (mailbox message)
  "Post the MESSAGE in the MAILBOX, notifying the waiting thread."
  (declare (stepper disable))
  (bt:with-lock-held ((%mailbox-lock mailbox))
    (setf (%mailbox-message mailbox) message
          (%mailbox-full mailbox) t)
    (bt:condition-notify (%mailbox-condition mailbox))))


;;; --------------------------------------------------------------------

(defun test/mailbox ()
  (let ((start (get-universal-time)))
    (assert (equal (let ((mb (make-mailbox)))
                     (bt:make-thread (lambda () (sleep 4) (mailbox-post mb 42)))
                     (mailbox-collect mb))
                   42))
    (let ((end (get-universal-time)))
      (assert (<= 3 (- end start) 5))))
  (let ((start (get-universal-time)))
    (assert (equal (let ((mb (make-mailbox)))
                     (bt:make-thread (lambda () (mailbox-post mb 42)))
                     (mailbox-collect mb))
                   42))
    (let ((end (get-universal-time)))
      (assert (<= 0 (- end start) 1))))
  :success)

;;;; THE END ;;;;
