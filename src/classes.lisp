(in-package :cl-naive-session)

;;; --------------------
;;; packet interface 
;;; --------------------

(deftype octet-vector () `(vector (unsigned-byte 8)))

(defclass packet ()
  ((parameters :initarg :parameters :type list
               :initform nil
               :accessor packet-parameters
               :documentation "A p-list of packet parameters.")
   (message    :initarg :message
               :initform nil
               :accessor packet-message
	       :documentation "A S-expression holding the packet message.")))

(defgeneric encode-packet (transport packet)
  (:documentation "Return an object that the transport can use to send the PACKET.
The actual type of the result depends on the transport."))

(defgeneric decode-packet (transport packet-data)
  (:documentation "Parses the PACKET-DATA and returns a decoded packet instance.
The actual type of PACKET-DATA depends on the transport."))



;;; --------------------
;;; transport interface 
;;; --------------------

(defclass multiple-sessions-mixin ()
  ((sessions :initform (make-hash-table :test (function equal))
             :reader transport-sessions
             :documentation "A map of session-id to session")))

(defgeneric get-session-by-id  (transport session-id))
(defgeneric unregister-session (transport old-session))
(defgeneric register-session   (transport new-session))


(defclass transport ()
  ((state :initform nil :accessor state)
   (secure :initform nil :initarg :secure :reader transport-secure))
  (:documentation "Abstract class for transports."))

(defclass client-transport-mixin ()
  ((remote-url :initarg :remote-url :initform nil :reader remote-url)))

(defclass server-transport-mixin ()
  ((local-url :initarg :local-url :initform nil :reader local-url)))

(defclass http-transport (transport)
  ((lock :initform (bt:make-lock) :reader transport-lock))
  (:documentation "Abstract class for HTTP-based transports."))

(defclass stream-transport (transport)
  ()
  (:documentation "Abstract class for stream-based transports."))



(defclass http-drakma-transport (client-transport-mixin http-transport)
  ((remote-stream :initform nil :accessor remote-stream)
   (request-queue :initform (make-queue "request") :reader request-queue)
   (response-queue :initform (make-queue "response") :reader response-queue)))

(defclass http-webserver-transport (multiple-sessions-mixin server-transport-mixin http-transport)
  ((webserver :initarg :webserver :reader webserver))
  (:documentation "A transport that uses a webserver to communicate with clients.
To create an instance, the webserver site must be passed as :site initarg
and the resource (e.g. \"/service\") as :resource initarg."))



(defmethod print-object ((transport client-transport-mixin) stream)
  (print-unreadable-object (transport stream :type t :identity t)
    (format stream "~A" (remote-url transport))
    transport))

(defmethod print-object ((transport server-transport-mixin) stream)
  (print-unreadable-object (transport stream :type t :identity t)
    (format stream "~A" (local-url transport))
    transport))




(defgeneric register (object))
(defgeneric unregister (object))

(defgeneric send-packet (endpoint packet))
(defgeneric receive-packet (endpoint))


;; (defgeneric ensure-connected-transport (transport))

;; (defgeneric did-connect (transport))
;; (defgeneric did-disconnect (transport))
;; 
;; (defgeneric connect (transport))
;; (defgeneric disconnect (transport))
;; 
;; 
;; (defgeneric did-receive (transport packet))



;;; ----------------------------------------
;;; session interfaces 
;;; ----------------------------------------

(defclass session ()
  ((id :initarg :id :initform nil :reader session-id)
   (state :initform nil :accessor state :reader session-state)
   (transport :initarg :transport :initform nil :accessor transport)
   (local-url :initarg :local-url :initform nil :reader session-local-url)
   (remote-url :initarg :remote-url :initform nil :reader session-remote-url)))

(defclass server-session (session)
  ((request-mailbox :initform (make-mailbox "request") :reader request-mailbox)
   (response-mailbox :initform (make-mailbox "response") :reader response-mailbox)))

(defclass client-session (session)
  ())

(defmethod print-object ((session session) stream)
  (print-unreadable-object (session stream :type t :identity t)
    (format stream "~A ~S ~S -> ~S"
            (session-id session)
            (session-state session)
            (session-local-url session)
            (session-remote-url session)))
  session)

(defgeneric delete-session (session))

;;; ----------------------------------------
;;; Session Internal Interfaces
;;; ----------------------------------------

(defgeneric merge-packets (session-packet user-packet &optional additionnal-parameters)
  (:documentation "Return a packet with merged data from 
session-packet and user-packet.
Since session packets only contain parameters, the packet message
is the user packet message. The parameters are just the concatenated 
parameters from both packets."))

(defgeneric remove-session-parameters (session-packet)
  (:documentation "Return a packet with the session parameters removed."))

(defgeneric session-request-create  (session))
(defgeneric session-request-delete  (session))
(defgeneric session-request-message (session))
(defgeneric session-response-ok     (session))
(defgeneric session-response-reject (session-id &optional message))

(defgeneric session-version (object)
  (:documentation "Return the session layer protocol version."))
(defgeneric session-id (object)
  (:documentation "Return the session layer session ID."))
(defgeneric session-command (object)
  (:documentation "Return the session layer command."))
(defgeneric session-status (object)
  (:documentation "Return the session layer status."))
(defgeneric session-message (object)
  (:documentation "Return the session layer message (when status = \"reject\"."))


;;;; THE END ;;;;
