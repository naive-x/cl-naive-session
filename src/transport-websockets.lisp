(in-package :cl-naive-session)

;; In this file, we define the methods that use cl-naive-websockets 
;; for the transport layer.  As a separate file, it can be used in a 
;; distinct system depending on cl-naive-websockets, while the main 
;; system does not.

(defclass websockets-drakma-transport (client-transport-mixin stream-transport)
  ((websockets-endpoint :initarg :websockets-endpoint
                        :reader websockets-endpoint)))

(defclass websockets-webserver-transport (multiple-sessions-mixin server-mixin-transport stream-transport)
  ((webserver :initarg :webserver :reader webserver)
   (site :initarg :site :reader site)
   (resource :initarg :resource :reader resource)
   (websockets-endpoint :initarg :websockets-endpoint
                        :reader websockets-endpoint))
  (:documentation "A transport that uses a websockets over webserver to communicate with clients.
To create an instance, the webserver site must be passed as :site initarg
and the resource (e.g. \"/service\") as :resource initarg."))


;;
;; The websockets client:
;;

(defmethod send-packet ((transport websockets-drakma-transport) (packet packet))
  (let ((endpoint   (websockets-endpoint transport))
        (parameters (packet-parameters packet))
        (payload    (packet-message packet)))
    (unless endpoint
      (let* ((uri               (puri:parse-uri (remote-url transport)))
             (host              (puri:uri-host uri))
             (port              (puri:uri-port uri))
             (secure            (secure-scheme-p (puri:uri-scheme uri)))
             (resource-name     (puri:uri-path uri)))
        (setf endpoint (setf (slot-value transport 'websockets-endpoint)
                             (cl-naive-websockets:websockets-connect
                              host resource-name
                              :port port
                              :secure secure
                              :subprotocols '("session")
                              :extensions '()
                              :origin nil
                              :additionnal-http-headers '()
                              :http-client-connect-function (function cl-naive-websockets.client.drakma:drakma-http-client-connect))))
        (setf (cl-naive-websockets:endpoint-upper-layer endpoint) transport)))
    (send-message endpoint parameters payload)))

;;
;; The websockets server:
;;

(defun session-ws-server (server endpoint)
  ;; This is the local-websockets-service websockets server loop
  ;; It's installed in instances of the service-ws-server class.
  (let ((transport (session-ws-server-transport server)))
    (setf (cl-naive-websockets:endpoint-upper-layer endpoint) transport)
    (log-message :info transport "Starting ~S loop" 'session-ws-server)
    (unwind-protect
         (loop
           :do (multiple-value-bind (status parameters request)
                   (receive-parameters-payload endpoint :s-status :s-payload)
                 (when (string= status "closed")
                   (loop-finish))
                 ;; TODO: handle transport/session stuff:
                 (handler-case

                     (let* ((packet (make-instance 'packet
                                                   :parameters parameters
                                                   :message    request))
                            (session (get-session-by-id transport (session-id packet))))
                       (when (and (null session)
                                  (equalp "create" (session-command packet)))
                         (setf session (get-session-by-id transport nil)))
                       (let* ((status "ok")
                              (response (if session
                                            (progn
                                              (mailbox-post (request-mailbox session) packet)
                                              ;; wait for a response in response-mailbox
                                              (mailbox-collect (response-mailbox session)))
                                            (progn
                                              ;; send a negative response
                                              (setf status "error")
                                              (session-response-reject (session-id packet)
                                                                       (if (session-id packet)
                                                                           "Session ID unknown."
                                                                           "Missing a Session ID in headers.")))))
                              (parameters (packet-parameters response))
                              (payload    (packet-message response)))

                         (if (string= status "ok")
                             (send-parameters-payload endpoint (list* :s-status status parameters) payload
                                                      :s-binary :s-payload)
                             (send-parameters-payload endpoint (list* :s-status "error" parameters) payload
                                                      :s-binary :s-payload))))

                   (error (err)
                     (log-message :error transport "Error handling the request: ~A" err)
                     (send-parameters-payload endpoint (list* :s-status "error" parameters) (format nil "~a" err)
                                              :s-binary :s-payload)))))
      ;; cleanup
      (handler-case
          (cl-naive-websockets:websockets-disconnect endpoint)
        (error (err)
          (log-message :error transport "Error disconnecting websockets: ~A" err))))))

(defclass session-ws-server (cl-naive-websockets:websockets-server)
  ((transport :initarg :transport :reader session-ws-server-transport))
  (:default-initargs
   :subprotocols '("session")
   :server-function (function session-ws-server)))

#|

SESSION-SITE-RESOURCE-MIXIN

Note: we want to select the websockets server upon receiving a request on site/resource.
But the websockets-acceptor-mixin runs before the site-acceptor-mixin (in hunchentoot-acceptor),
and its acceptor-dispatch-request method just calls accept-websockets-negociation, with no filter.

So we need this session-site-resource-mixin to filter both on the site and the resource-name.
This means that the site-acceptor-mixin test becomes useless for the same site and resource-name,
so the handler produced by make-websockets-handler should never be used.

|#

(defclass session-site-resource-mixin ()
  ((site          :initarg :site          :reader session-site)
   (resource-name :initarg :resource-name :reader session-resource-name))
  (:documentation "This mixin let us filter by site and resource-name."))

(defmethod hunchentoot:acceptor-dispatch-request ((acceptor session-site-resource-mixin) request)
  (log-message :warn acceptor
               "script-name   = ~S~%resource-name = ~S~%"
               (hunchentoot:script-name request) (session-resource-name acceptor))
  (if (and ;; same site or sites with same url:
       (or (eql (session-site acceptor) cl-naive-webserver:*site*)
           (equalp (cl-naive-webserver:url (session-site acceptor))
                   (cl-naive-webserver:url cl-naive-webserver:*site*)))
       ;; same resource name:
       (string= (hunchentoot:script-name request) (session-resource-name acceptor))
       ;; is a websockets session:
       (cl-naive-websockets:accept-websockets-negociation acceptor request))
      ;; then: it was a websocket request:
      (values "" nil nil)
      ;; else: defer to the other mixins or superclasses:
      (when (next-method-p)
        (call-next-method))))

(defclass session-acceptor (session-site-resource-mixin
                            cl-naive-websockets::websockets-acceptor-mixin
                            cl-naive-webserver.hunchentoot:hunchentoot-acceptor)
  ;; hunchentoot-acceptor isa (exclusion-acceptor-mixin
  ;;                           token-acceptor-mixin
  ;;                           session-acceptor-mixin
  ;;                           site-acceptor-mixin
  ;;                           hunchentoot:acceptor)
  ())

(defclass session-ssl-acceptor (session-site-resource-mixin
                                cl-naive-websockets::websockets-acceptor-mixin
                                hunchentoot:ssl-acceptor
                                cl-naive-webserver.hunchentoot:hunchentoot-acceptor)
  ;; TODO
  ())

(defun install-websocket-server (transport)
  "Creates a websockets-server, thru a webserver with a hunchentoot backend.
Return the new acceptor = hunchentoot server."
  (let* ((webserver-server  (webserver transport))
         (site              (site      transport))
         (resource          (resource  transport))
         (websockets-server (make-instance 'session-ws-server
                                           :transport transport)))
    ;;       ;; (request-class webserver-server) 'hunchentoot:acceptor-request

    ;; TODO: we clobber the acceptor of the webserver.  Perhaps webservers need a way to combine different acceptors for different sites and resources?
    
    (setf (cl-naive-webserver.hunchentoot:acceptor webserver-server) ; = acceptors are hunchentoot server
          (make-instance (if (transport-secure transport)
                             'session-ssl-acceptor
                             'session-acceptor)
                         :port (cl-naive-webserver:port webserver-server)
                         :address (cl-naive-webserver:address webserver-server)
                         :server webserver-server
                         :site site
                         :resource-name resource
                         :access-log-destination  (log-stream :info transport)
                         :message-log-destination (log-stream :warn transport)
                         :websockets-server websockets-server))))

(defun make-websockets-handler (transport)
  (declare (ignore transport))
  (lambda (script-name)
    ;; Should not be called, see session-site-resource-mixin
    (error "Internal error: this resource at ~A~A defined in ~S should not be called."
           (cl-naive-webserver:url cl-naive-webserver:*site*)
           script-name
           'make-websockets-handler)))

(defmethod initialize-instance :after ((transport websockets-webserver-transport) &key webserver site resource &allow-other-keys)

  ;; Validate mandatory &key arguments:
  (assert (typep webserver 'cl-naive-webserver:server) (webserver)
          "A ~S needs a webserver:server :webserver parameter, instead of ~S"
          'websockets-webserver-transport webserver)
  (assert (typep site 'cl-naive-webserver:site) (site)
          "A ~S needs a webserver:site :site parameter, instead of ~S"
          'websockets-webserver-transport site)
  (assert (stringp resource) (resource)
          "A ~S needs a string :resource parameter, instead of ~S"
          'websockets-webserver-transport resource)

  ;; Initialize the transport:
  (install-websocket-server transport)

  ;; Just in case we fall thru to the site mixin:
  (let ((site (if (stringp site)
                  (cl-naive-webserver:get-site site)
                  site)))
    (add-handler site resource (make-websockets-handler transport))))

(eval-when (:compile-toplevel :load-toplevel :execute)
  (pushnew '(:wss   websockets-drakma-transport    t)    *client-transports* :test (function equalp))
  (pushnew '(:ws    websockets-drakma-transport    nil)  *client-transports* :test (function equalp))
  (pushnew '(:wss   websockets-webserver-transport t)    *server-transports* :test (function equalp))
  (pushnew '(:ws    websockets-webserver-transport nil)  *server-transports* :test (function equalp)))

;;;; THE END ;;;;
