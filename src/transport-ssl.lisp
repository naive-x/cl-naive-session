(in-package :cl-naive-session)

;; In this file, we define the methods that use cl-naive-websockets 
;; for the transport layer.  As a separate file, it can be used in a 
;; distinct system depending on cl-naive-websockets, while the main 
;; system does not.



(defclass ssl-client-transport (stream-transport)
  ((remote-url :initarg :remote-url :reader remote-url)))

(defclass ssl-server-transport (multiple-sessions-mixin stream-transport)
  ((local-url :initarg :local-url :reader local-url)))



(defmethod send-packet ((transport ssl-client-transport) (packet packet))
  (error "Not implemented yet"))


(setf *client-transports* (append *client-transports* '((:ssl   ssl-client-transport t)))
      *server-transports* (append *server-transports* '((:ssl   ssl-server-transport t))))

;;;; THE END ;;;;
