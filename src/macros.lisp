(in-package :cl-naive-session)

(defmacro iscase (keyform &body clauses)
  "
DO:         A CASE, but for case insensitive string-designator keys.
            That is, it uses STRING-EQUAL as test instead of the EQL test.
"
  (let ((key (gensym "KEY")))
    `(let ((,key ,keyform))
       (cond
         ,@(mapcar
            (lambda (clause)
              (if (or (eq (car clause) 'otherwise) (eq (car clause) 't))
                  `(t ,@(cdr clause))
                  `(,(typecase (car clause)
                       ((or string symbol character)
                        `(string-equal ,key ',(car clause)))
                       (list
                        `(member       ,key ',(car clause)
                                       :test (function string-equal)))
                       (t
                        (error "Bad ICASE clause: ~S~% expected type STRING-DESIGNATOR or LIST of STRING-DESIGNATOR" clause)))
                    ,@(cdr clause))))
            clauses)))))
