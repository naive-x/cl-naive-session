(defsystem "cl-naive-session"
  :description "Session layer over webserver, websocket, etc."
  :version "2023.10.24"
  :author "Phil Marneweck <phil@psychedelic.co.za>"
  :licence "MIT"
  :depends-on (:drakma
	       :bordeaux-threads
               :alexandria
	       :cl-naive-queue
               :cl-naive-webserver.hunchentoot
               :cl-naive-websockets.wrapper)
  :components (
	       (:file "src/package")
               (:file "src/macros"               :depends-on ("src/package"))
	       (:file "src/mailbox"              :depends-on ("src/package"))
               (:file "src/queue"                :depends-on ("src/package"))
               (:file "src/classes"              :depends-on ("src/package"
                                                              "src/macros"))
               (:file "src/conditions"           :depends-on ("src/package"))
               (:file "src/packet"               :depends-on ("src/package"
                                                              "src/macros"
                                                              "src/classes"))
               (:file "src/transport"            :depends-on ("src/package"
                                                              "src/macros"
                                                              "src/classes"
                                                              "src/mailbox"
                                                              "src/queue"))
               (:file "src/session"              :depends-on ("src/package"
                                                              "src/macros"
	                                                      "src/classes"
                                                              "src/conditions"
                                                              "src/packet"
	                                                      "src/transport"))))
