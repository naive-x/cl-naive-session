(defsystem "cl-naive-session.example.microservice.adder-client"
  :description "Example microservice adder client."
  :version "2023.8.2"
  :author "Phil Marneweck <phil@psychedelic.co.za>"
  :licence "MIT"
  :depends-on (:drakma
               :cl-naive-session)
  :components ((:file "examples/adder-client")))
