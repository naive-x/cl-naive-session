(eval-when (:compile-toplevel :load-toplevel :execute)
  (pushnew (truename (merge-pathnames
                      (make-pathname :name nil :type nil :version nil
                                     :defaults (or *load-truename*
                                                   *compile-file-truename* #P"./"))
	              #P"../"))
           ql:*local-project-directories*
           :test #'equalp)
  (ql:quickload '(:cl-naive-webserver.hunchentoot
                 :cl-naive-who-ext
                 :drakma)))


(defpackage :naive-examples (:use :cl :cl-naive-webserver))
(in-package :naive-examples)



(defpackage :cl-naive-session.example
  (:use :common-lisp :cl-naive-log :cl-naive-webserver))
(in-package :cl-naive-session.example)

(defvar *port* 33034)

(defparameter *server*
  (make-instance 'cl-naive-webserver.hunchentoot:hunchentoot-server
                 :id 'simple
		 :address "localhost"
		 :port *port*))
(start-server *server*)



(defparameter *simple-site*
  (register-site (make-instance 'site :url "/simple")))

(setf (cl-naive-webserver:find-resource *simple-site* "/example")
      (lambda (script-name)
	(declare (ignore script-name))
        "Yeeeha she is a live and kicking!!"))

(assert (equal (drakma:http-request
          "http://localhost:33034/simple/example"
          :method :get)
         "Yeeeha she is a live and kicking!!"))

;; (setf *no-session-p* t)
;; (drakma:http-request
;;  "http://localhost:3333/example"
;;  :method :get)
;; 
;; ;;Try the following in your browser it should give you an authorization error
;; 
;; ;;http://localhost:3333/example
;; 
;; ;;Get a token issued first
;; 
;; ;;http://localhost:3333/token
;; 
;; ;;No try /example again
;; 
;; ;;http://localhost:3333/example


;; (defparameter *ms-site*
;;   (register-site (make-instance 'site :url "/ms")))

;; (setf (cl-naive-webserver:find-resource *ms-site* "/foo")
;;       (lambda (script-name)
;; 	(declare (ignore script-name))
;;         (process-message (extract-session *request*)
;;                          (extract-message *request*))))




(defun ms-ws-server (server endpoint)
  (declare (ignore server))

  ;; This is an example of synchronous loop,
  ;; where the server takes a message, compute an answer (with process-message)
  ;; and sends a response back to the client.

  ;; But we may have more complex scenarios, where the server
  ;; may send messages to the client without receiving anything from it.
  ;; In this case, we may want to use a thread to send messages to the client
  ;; and another thread to receive messages from the client.
  
  (with-debugging
    (unwind-protect
         (loop
           :initially (send-text-message endpoint "HELLO I will echo text and binary messages")
           :for message := (receive-message endpoint)
           :do (with-output-to-string (*standard-output*)
                 (typecase message
                   (websockets-message-fragment
                    ;; (log-message :info server "Server Received a message fragment ID ~A opcode ~A ~S (~A bytes) ~S"
                    ;;         (message-fragment-id message)
                    ;;         (message-fragment-opcode message)
                    ;;         (message-fragment-position message)
                    ;;         (length (message-fragment-data message))
                    ;;         (message-data message))
                    ;; (when (eql :final (message-fragment-position message))
                    ;;   (send-text-message endpoint "Got last fragment."))
                    )
                   (websockets-close-message
                    ;; (let ((data (message-data message)))
                    ;;   (if (< 2 (length data))
                    ;;       (log-message :info server  "Server Received a close control message ~A ~S"
                    ;;                   (close-message-status message)
                    ;;                   (close-message-reason message))
                    ;;       (log-message :error server  "Server Received an illegal close control message")))
                    (loop-finish))
                   (websockets-pong-message
                    ;; (log-message :info server  "Server Received a pong control message ~A ~S"
                    ;;         (message-elapsed-time message) (message-data message))
                    )
                   (websockets-ping-message
                    ;; (log-message :info server  "Server Received a ping control message ~S" (message-data message))
                    )

                   (websockets-text-message
                    (log-message :info server  "Server Received a text message ~S" (message-string message))

                    (send-text-message endpoint
                                       (prepare-text-message
                                        (process-message (extract-session-from-text message)
                                                         (extract-message-from-text message*)))))

                   (websockets-binary-message
                    (log-message :info server "Server Received a binary message ~S" (message-data message))

                    (send-binary-message endpoint
                                       (prepare-binary-message
                                        (process-message (extract-session-from-binary (message-data message))
                                                         (extract-message-from-binary (message-data message))))))

                   (t
                    (log-message :warn server  "Server Received something of type ~S: ~S" (type-of message) message)
                    (ignore-errors (websockets-disconnect endpoint :reason "Received an unknown message type" :status +ws-protocol-error+))
                    (loop-finish)))))
      
      (ignore-errors (websockets-disconnect endpoint)))))
