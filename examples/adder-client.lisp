(in-package :cl-user)
(defpackage :example.microservice.adder-client
  (:use
   :common-lisp
   :cl-naive-session
   :drakma)
  (:documentation "This package defines a simple CLI client for the adder microservice")
  (:export :main))
(in-package :example.microservice.adder-client)

(defmacro scase (keyform &rest clauses)
  "
DO:         A CASE, but for string keys. That is, it uses STRING= as test
            instead of the ''being the same'' test.
"
  (let ((key (gensym "KEY")))
    `(let ((,key ,keyform))
       (cond
         ,@(mapcar (lambda (clause)
                     (if (or (eq (car clause) 'otherwise) (eq (car clause) 't))
                         `(t ,@(cdr clause))
                         `((member ,key ',(car clause) :test (function string=))
                           ,@(cdr clause))))
                   clauses)))))


(defvar *default-port* 8080)
(defvar *default-address* "localhost")


(defun adder-request (session numbers)
  (send session (make-instance 'packet :message (list* :add numbers)))
  (let ((response (receive session)))
    (scase (getf (packet-parameters response) :add-status)
      (("ok") (format t "~&Result: ~a~%" (packet-message response)))
      (("error") (error (packet-message response)))
      (otherwise (error "Invalid response from server: ~a" response)))))


(defun client-loop (session)
  (loop
    :do (format t "~&Enter numbers to add, end with an empty line: ")
        (force-output)
        (let ((numbers (loop :for line := (string-trim #(#\Space #\Tab #\Newline)
                                                       (read-line *standard-input* nil ""))
                             :while (not (equal "" line))
                             :if (equal "quit" line) :do (return-from client-loop nil)
                             :else :append (with-standard-io-syntax
                                             (with-input-from-string (stream line)
	                                           (loop :for item := (read stream nil nil)
                                                     :while item
                                                     :when (numberp item) :collect item))))))
          (handler-case
              (adder-request session numbers)
            (error (err)
              (format t "~&Error: ~a~%" err))))))


(defvar *session* nil)

(defun run-client (port numbers)
  (let ((url (format nil "http://~A:~A/ms/add" *default-address* port)))
    (setf *session* (open-session url))
    (unwind-protect
         (if numbers
             (adder-request *session* numbers)
             (client-loop *session*))
      (close-session *session*))))

(defun main (pname &rest arguments)
  (handler-case
      (let ((port 8080)
            (numbers '()))
        (loop :while arguments
              :do (let ((arg (pop arguments)))
                    (scase arg
                      (("--port" "-p")
                       (setf port (parse-integer (pop arguments)))
                       (unless (<= 1024 port 32767)
                         (error "Invalid port number: ~a" port)))
                      (("--help" "-h")
                       (format t "~&Usage: ~a [--port PORT]~%" pname)
                       (finish-output)
                       (return-from main 0))
                      (otherwise
                       (let ((item (handler-case (with-standard-io-syntax
                                                   (read-from-string arg))
                                     (error ()
                                       (error "Invalid syntax for a number argument: ~a" arg)))))
                         (unless (numberp item)
                           (error "Invalid syntax for a number argument: ~a" arg))
                         (push item numbers)))))
              :finally (setf numbers (nreverse numbers)))
        (run-client port numbers))

    (error (err)
      (format t "~&Error: ~a~%" err)
      (return-from main 1))))

