(in-package :cl-user)
(defpackage :example.microservice.adder
  (:use
   :common-lisp
   :cl-naive-session
   :cl-naive-webserver)
  (:shadowing-import-from :cl-naive-session :session)
  (:documentation "This package defines a simple microservice that adds two numbers together.")
  (:export :main))
(in-package :example.microservice.adder)

(defmacro scase (keyform &rest clauses)
  "
DO:         A CASE, but for string keys. That is, it uses STRING= as test
            instead of the ''being the same'' test.
"
  (let ((key (gensym "KEY")))
    `(let ((,key ,keyform))
       (cond
         ,@(mapcar (lambda (clause)
                     (if (or (eq (car clause) 'otherwise) (eq (car clause) 't))
                         `(t ,@(cdr clause))
                         `((member ,key ',(car clause) :test (function string=))
                           ,@(cdr clause))))
                   clauses)))))

;;; Webserver

(defvar *default-port* 8080)
(defvar *default-address* "localhost")

(defun create-server (&key (port *default-port*) (address *default-address*))
  (let ((server (make-instance 'cl-naive-webserver.hunchentoot:hunchentoot-server
		               :address address
		               :port port)))
    (register-server server)
    (start-server server)
    server))

(defun create-site (url)
  (let ((site (make-instance 'site :url url)))
    (register-site site)
    site))

(defmethod add-handler ((site site) (subpath string) (handler function))
  (setf (cl-naive-webserver:find-resource site subpath) handler))

;;; Add microservice

(defun process-add-request (numbers)
  (let ((sum (reduce (function +) numbers)))
    sum))

(defun microservice-loop (session first-message)
  (loop
    :until (eq :deleted (session-state session))
    :do (let* ((message (or (prog1 first-message (setf first-message  nil))
                            (receive session)))
               (request (packet-message message)))
          (cond
            ((eq :deleted (session-state session)))
            ((and (listp request)
                  (equal :add (first request))
                  (every (function numberp)
                         (rest request)))
             (handler-case
                 (let ((result (process-add-request (rest request))))
                   (send session (make-instance 'packet
                                                :parameters (list :request request
                                                                  :add-status "ok")
                                                :message result)))
               (error (err)
                 (send session (make-instance 'packet
                                              :parameters (list :request request
                                                                :add-status "error")
                                              :message (format nil "~a" err))))))
            (t
             (send session (make-instance 'packet
                                          :parameters (list :request request
                                                            :add-status "error")
                                          :message "Invalid request.")))))))


(defvar *server* nil)
(defvar *ms-site* nil)


(defun run-microservice (port)
  (setf *server*  (create-server :port port))
  (setf *ms-site* (create-site "/ms"))
  (let ((url (format nil "http://~A:~A/ms/add" *default-address* port)))
    (create-server-transport url :site *ms-site* :resource "/add")
    (format t "Microservice running at ~A~%" url)
    (force-output)
    (loop
       (let* ((session (accept-session url))
              (message (receive session)))
         ;; TODO: currently we need to call receive to be listening for a new connection.
         ;; 
         ;; TODO: shouldn't we call thread-join.
         (bt:make-thread (lambda () (microservice-loop session message))
                         :name (session-id session))))))


(defun main (pname &rest arguments)
  (handler-case

      (let ((port 8080))
        (loop :while arguments
              :do (let ((arg (pop arguments)))
                    (scase arg
                      (("--port" "-p")
                       (setf port (parse-integer (pop arguments)))
                       (unless (<= 1024 port 32767)
                         (error "Invalid port number: ~a" port)))
                      (("--help" "-h")
                       (format t "~&Usage: ~a [--port PORT]~%" pname)
                       (finish-output)
                       (return-from main 0))
                      (otherwise
                       (error "Invalid argument: ~a" arg)))) )
        (run-microservice port))

    (usocket:address-in-use-error (err)
      (format *error-output* "~&Error while starting the server: ~A~%" err)
      (return-from main 1))
    
    (error (err)
      (format t "~&Error: ~a~%" err)
      (return-from main 1))))

