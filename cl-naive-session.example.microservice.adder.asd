(defsystem "cl-naive-session.example.microservice.adder"
  :description "Example microservice adder."
  :version "2023.8.3"
  :author "Phil Marneweck <phil@psychedelic.co.za>"
  :licence "MIT"
  :depends-on (:cl-naive-webserver.hunchentoot
               :cl-naive-session)
  :components ((:file "examples/adder")))
